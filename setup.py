from setuptools import setup
from gitubuntu.version import VERSION

setup(name='gitubuntu',
    version=VERSION,
    packages=['gitubuntu'],
    entry_points={
        'console_scripts': [
            'git-ubuntu = gitubuntu.__main__:main'
        ],
    },
    include_package_data=True,
	url="https://code.launchpad.net/git-ubuntu",
	maintainer="Ubuntu Server Team",
	maintainer_email="ubuntu-distributed-devel@lists.ubuntu.com",

    install_requires=[
        'argcomplete',
        'cachetools',
        'python-debian',
        'pygit2',
        'launchpadlib',
        'petname',
        'pylint',
        'SecretStorage',
        'setuptools',
        'keyring',
        'keyrings.alt',
        'lazr.restfulclient',
        'pytest',
        'pytest-cov',
        'systemd-python',
        'tenacity',
        'ubuntutools',
        'zmq',
    ]
)
