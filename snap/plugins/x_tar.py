from snapcraft.plugins import autotools

class XTarPlugin(autotools.AutotoolsPlugin):
    def env(self, root):
        env = super().env(root)
        env.extend([
            'FORCE_UNSAFE_CONFIGURE=1',
        ])
        return env
