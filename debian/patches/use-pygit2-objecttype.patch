From: Andreas Hasenack <andreas.hasenack@canonical.com>
Date: Sat, 14 Dec 2024 11:30:24 -0300
Subject: Replace deprecated GIT_OBJ_XXX references

Origin: vendor
Bug-Debian: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1084258
Bug-Ubuntu: https://bugs.launchpad.net/ubuntu/+source/git-ubuntu/+bug/2091778
---
 gitubuntu/git_repository.py      | 13 +++++++------
 gitubuntu/repo_builder.py        |  3 ++-
 gitubuntu/source_builder_test.py |  3 ++-
 3 files changed, 11 insertions(+), 8 deletions(-)

diff --git a/gitubuntu/git_repository.py b/gitubuntu/git_repository.py
index 27ed645..c177198 100644
--- a/gitubuntu/git_repository.py
+++ b/gitubuntu/git_repository.py
@@ -34,6 +34,7 @@ import gitubuntu.versioning
 import debian.changelog
 import debian.debian_support
 import pygit2
+from pygit2.enums import ObjectType
 import pytest
 
 
@@ -65,7 +66,7 @@ def _follow_symlinks_to_blob(repo, top_tree_object, search_path,
         tail = None
 
     entry = _rel_tree[head]
-    if entry.type in [pygit2.GIT_OBJ_TREE, 'tree']:
+    if entry.type in [ObjectType.TREE, 'tree']:
         return _follow_symlinks_to_blob(
             repo=repo,
             top_tree_object=top_tree_object,
@@ -73,7 +74,7 @@ def _follow_symlinks_to_blob(repo, top_tree_object, search_path,
             _rel_tree=repo.get(entry.id),
             _rel_path=posixpath.join(_rel_path, head),
         )
-    elif entry.type in [pygit2.GIT_OBJ_BLOB, 'blob'] and entry.filemode == pygit2.GIT_FILEMODE_LINK:
+    elif entry.type in [ObjectType.BLOB, 'blob'] and entry.filemode == pygit2.GIT_FILEMODE_LINK:
         # Found a symlink. Start again from the top with adjustment for symlink
         # following
         target_tail = [decode_binary(repo.get(entry.id).data)]
@@ -87,7 +88,7 @@ def _follow_symlinks_to_blob(repo, top_tree_object, search_path,
             top_tree_object=top_tree_object,
             search_path=search_path,
         )
-    elif entry.type in [pygit2.GIT_OBJ_BLOB, 'blob'] and entry.filemode in NORMAL_BLOB_MODES:
+    elif entry.type in [ObjectType.BLOB, 'blob'] and entry.filemode in NORMAL_BLOB_MODES:
         return repo.get(entry.id)
     else:
         # Found some special entry such as a "gitlink" (submodule entry)
@@ -2111,7 +2112,7 @@ class GitUbuntuRepository:
         self.raw_repo.create_tag(
             tag_name,
             pygit2.Oid(hex=commit_hash),
-            pygit2.GIT_OBJ_COMMIT,
+            ObjectType.COMMIT,
             tagger,
             tag_msg,
         )
@@ -2328,7 +2329,7 @@ class GitUbuntuRepository:
         empty TreeBuilder instead.
         '''
 
-        tree = treeish.peel(pygit2.GIT_OBJ_TREE)
+        tree = treeish.peel(ObjectType.TREE)
 
         # Short path: sub_path == '' means want root
         if not sub_path:
@@ -2342,7 +2343,7 @@ class GitUbuntuRepository:
         else:
             # The tree entry must itself be a tree
             assert tree_entry.filemode == pygit2.GIT_FILEMODE_TREE
-            sub_tree = repo.get(tree_entry.id).peel(pygit2.GIT_OBJ_TREE)
+            sub_tree = repo.get(tree_entry.id).peel(ObjectType.TREE)
             tree_builder = repo.TreeBuilder(sub_tree)
 
         return tree_builder
diff --git a/gitubuntu/repo_builder.py b/gitubuntu/repo_builder.py
index 62a7dbc..2147922 100644
--- a/gitubuntu/repo_builder.py
+++ b/gitubuntu/repo_builder.py
@@ -2,6 +2,7 @@ import binascii
 import copy
 
 import pygit2
+from pygit2.enums import ObjectType
 
 import gitubuntu.importer
 from gitubuntu.source_builder import Source, SourceSpec
@@ -386,7 +387,7 @@ class Repo:
             repo.create_tag(
                 name,
                 target.write(repo),
-                pygit2.GIT_OBJ_COMMIT,
+                ObjectType.COMMIT,
                 self.tagger.signature,
                 'Tag message',
             )
diff --git a/gitubuntu/source_builder_test.py b/gitubuntu/source_builder_test.py
index ab36985..a373223 100644
--- a/gitubuntu/source_builder_test.py
+++ b/gitubuntu/source_builder_test.py
@@ -3,6 +3,7 @@ import subprocess
 import tempfile
 
 import pygit2
+from pygit2.enums import ObjectType
 import pytest
 
 import gitubuntu.source_builder as target
@@ -141,7 +142,7 @@ def test_source_quilt_no_patches(repo):
     with target.Source(target.SourceSpec()) as dsc_path:
         top = dsc_path_to_tree(repo, dsc_path)
         debian_entry = top['debian']
-        assert debian_entry.type in [pygit2.GIT_OBJ_TREE, 'tree']
+        assert debian_entry.type in [ObjectType.TREE, 'tree']
         debian = repo.raw_repo.get(debian_entry.id)
         with pytest.raises(KeyError):
             debian['patches']
