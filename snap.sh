#!/bin/sh
set -e

# This script is a workaround for LP: #1918968. A deb is built, added to an apt
# repository, snapcraft.yaml adjusted to add a new "package-repositories"
# section pointing to it, and then snapcraft is run to generate the snap based
# on it.

# NOTE: THIS SCRIPT IS "DESTRUCTIVE" TO THE HOST
#
# It will install deb and snap dependencies using "sudo" on your system as well
# as install a temporariy git-ubuntu deb locally as built from this source tree
# and add sources and keys to apt for this purpose. It is intended for use in a
# temporary VM environment only.

tmp_dir=$(mktemp -d)
cleanup() { rm -rf "$tmp_dir"; }
trap cleanup EXIT

# Install dependencies of this script
#     apt-utils: needed for apt-ftparchive
#     devscripts: needed for mk-build-deps and dch
#     equivs: needed by mk-build-deps
#     python3 and python3-yaml: needed to modify snapcraft.yaml
sudo apt-get update
sudo apt-get install -y --no-install-recommends \
    apt-utils \
    devscripts \
    equivs \
    python3 \
    python3-yaml
sudo snap install --classic snapcraft

# Set up debian/changelog with an appropriate version string
snap_version=$(git describe)
# Replace the hyphens appropriately to construct a valid Debian native version
# string
deb_version=$(echo $snap_version|sed 's/-/+/;s/-/./g')

export DEBEMAIL=ubuntu-devel-discuss@lists.ubuntu.com
export DEBFULLNAME="git-ubuntu snap.sh build script"
dch -v "$deb_version" ''
dch -r -D focal ''

# Install deb build dependencies and build the deb
mk-build-deps
# --allow-downgrades is helpful for development of this script because it
# ensures that the build dependency package gets updated regardless of the
# previous version used. In production it is a no-op.
sudo apt-get install -y --allow-downgrades ./git-ubuntu-build-deps_${deb_version}_all.deb
debian/rules build
fakeroot debian/rules binary

# Workaround for LP: #1918969: temporary apt repository signing
export GNUPGHOME="$tmp_dir/gpg"
mkdir -m700 "$GNUPGHOME"
# Generate the key without a subkey to work around
# https://github.com/snapcore/snapcraft/issues/4224 - but even with that fixed,
# there is no reason to generate a subkey as apt doesn't require one.
gpg --batch --gen-key <<EOF
%no-protection
Key-Type:1
Key-Length:2048
Name-Real: git-ubuntu build script apt repo
Name-Email: ubuntu-devel-discuss@lists.ubuntu.com
Expire-Date:0
EOF
key_id=$(gpg --list-keys --batch --with-colons|awk -F: '$1=="fpr"{print $10;exit}')
mkdir -p snap/keys
# Use the final 8 characters of the key fingerprint as required by snapcraft;
# see https://snapcraft.io/docs/package-repositories#heading--keyid and
# LP: #1918967.
gpg --batch -a -o snap/keys/$(expr substr "$key_id" 33 40).asc --export

build_dir=$(pwd)
repo_dir="$tmp_dir/repo"
mkdir "$repo_dir"
cd "$repo_dir"
mv "$build_dir/../git-ubuntu_${deb_version}_all.deb" .
apt-ftparchive packages . | tee Packages | gzip > Packages.gz
apt-ftparchive release . > Release
gpg --yes -o Release.gpg -ab Release
cd "$build_dir"

# Update snapcraft.yaml with a pointer to our temporary apt repository
python3 <<EOT
import yaml
with open('snap/snapcraft.yaml') as f:
    snapcraft = yaml.safe_load(f)
# The version is tweaked here to avoid the snap version being marked "dirty"
# just because we are modifying snapcraft.yaml with this script.
snapcraft['version'] = '$snap_version'
snapcraft['package-repositories'] = [{
    'type': 'apt',
    'url': 'file://' + '$repo_dir',
    'path': '.',
    'key-id': '$key_id',
}]
with open('snap/snapcraft.yaml', 'w') as f:
    yaml.dump(snapcraft, f)
EOT

# Build the snap itself
snapcraft --destructive-mode
