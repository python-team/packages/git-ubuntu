from collections import namedtuple
import os
import sys

import pkg_resources

# We expect to be running from a git repository in master for this
# script, because the snap's python code is not exposed except within
# the snap
try:
    REALPATH = os.readlink(__file__)
except OSError:
    REALPATH = __file__
sys.path.insert(
    0,
    os.path.abspath(
        os.path.join(os.path.dirname(REALPATH), os.path.pardir)
    )
)

Defaults = namedtuple(
    'Defaults',
    [
        'num_workers',
        'denylist',
        'dry_run',
        'use_allowlist',
        'reimport',
    ],
)

DEFAULTS = Defaults(
    num_workers=10,
    denylist=pkg_resources.resource_filename(
        'gitubuntu',
        'source-package-denylist.txt',
    ),
    dry_run=False,
    use_allowlist=True,
    reimport=False,
)
