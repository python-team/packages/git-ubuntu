import os

import pkg_resources

import gitubuntu.git_repository


def get_test_changelog(name):
    """Create a Changelog object from a test case pkg_resources file

    Find the test case file in the source tree or shipped with the package,
    read it into a Changelog object, and return that object.

    :param str name: the name of the test case changelog file from the
        gitubuntu/changelog_tests/ directory in the source tree.
    :returns: the Changelog object constructed from the test case input file
    :rtype: gitubuntu.git_repository.Changelog
    """
    return gitubuntu.git_repository.Changelog.from_path(os.path.join(
        pkg_resources.resource_filename('gitubuntu', 'changelog_tests'),
        name,
    ))
