import enum

PatchState = enum.Enum('PatchState', ['APPLIED', 'UNAPPLIED'])
