# Import time exceeds timeout
linux
linux-aws
linux-azure
linux-gcp
linux-oracle
linux-raspi2

# uses up too much disk
firefox
libreoffice
libreoffice-l10n
openoffice.org
oxide-qt
texlive-base
texlive-extra
texlive-lang
thunderbird

# LP: #1858653
# pkgbinarymangler 70~lucid1 is affected
pkgbinarymangler

# LP: #1987415
# combat 0.8.1-1 is affected
combat

# Edge case failures from mass universe import starting 2022-08-22 that aren't
# already mentioned above
0ad-data
chromium-browser
flightgear-data
freedoom
gcc-snapshot
hedgewars
insighttoolkit4
kicad-packages3d
libfpdi-php
maitreya
mame
metastudent-data
musescore-general-soundfont
notebook
ns3
opencv
openshot
oxref
oxygen-icons5
paraview
parmap
pcbasic
pgcli
praat
pymatgen
python-suntime
qgis
qtwebengine-opensource-src
quixote
rcolorbrewer
rinse
routino
u-boot-linaro
unidic-mecab
webcheck
widelands
xxsds-dynamic
