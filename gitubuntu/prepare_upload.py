"""Prepare an upload by pushing rich history

This subcommand provides two further subcommands: "args" and "mangle". Both
subcommands first "git fetch" and then "git push" if required so that the rich
history is made available to the importer when it later imports your upload.
They then diverge in how they provide a pointer to that rich history.

The "args" subcommand prints command line arguments suitable to provide to
another command that will generate the changes file. It defaults to printing
output suitable for passing to "dpkg-buildpackage".

The "mangle" subcommand mangles an existing changes file to add the required
fields. It requires a single positional argument pointing to the changes file
to mangle.
"""
import argparse
import collections
import os
import re
import sys

import debian.deb822
import pygit2

import gitubuntu.importer
import gitubuntu.git_repository


# The user needs to include Vcs-Git, Vcs-Git-Ref and Vcs-Git-Commit headers in
# their changes file in order for the git-ubuntu importer service to later find
# the rich history pushed by this command. The following format strings provide
# the expansions needed for users of the different build commands.
OUTPUT_FORMATS = {
    'dpkg-buildpackage': "--changes-option=-DVcs-Git={Vcs-Git}"
        " --changes-option=-DVcs-Git-Ref={Vcs-Git-Ref}"
        " --changes-option=-DVcs-Git-Commit={Vcs-Git-Commit}",
    'sbuild': "--debbuildopt=--changes-option=-DVcs-Git={Vcs-Git}"
        " --debbuildopt=--changes-option=-DVcs-Git-Ref={Vcs-Git-Ref}"
        " --debbuildopt=--changes-option=-DVcs-Git-Commit={Vcs-Git-Commit}",
}

# Rewrite rule for VCS URLs. See LP: #1942985
URL_REWRITE = (
    # From
    # https://git.launchpad.net/launchpad/tree/lib/lp/app/validators/name.py,
    # valid characters in Launchpad usernames are a-z, 0-9, +, . and -. There
    # are other constraints, but for our purposes they don't matter. We aren't
    # using this for validation; only to ensure that we match all valid
    # Launchpad usernames, but don't accidentally match non-Launchpad URLs. We
    # also deliberately include capitals in case they are treated
    # case-insensitively further down the line, so as to not accidentally be
    # more restrictive than necessary.
    re.compile(r'^(?:git\+)?ssh://(?:[A-Za-z0-9\+\.\-]+@)?git\.launchpad\.net/'),
    'https://git.launchpad.net/',
)


class ForcePushRequired(RuntimeError): pass


def _add_subparser_arguments(parser):
    """Load standard prepare-upload arguments into the given parser

    This is so that we can do this for the general prepare-upload subparser, as
    well as the specific args and mangle subparsers, so the arguments work
    regardless of whether they are placed before or after the final args/mangle
    subcommand. See: https://stackoverflow.com/a/46962350/478206

    :param argparse.ArgumentParser parser: the parser to which toadd the
        arguments.
    """
    parser.add_argument('--remote', type=str, help="Remote to push to")
    parser.add_argument('--branch', type=str, help="Branch to push")
    parser.add_argument('--force-push', action='store_true')


def parse_args(subparsers=None, base_subparsers=None):  # pragma: no cover
    kwargs = dict(
        description="Push branch and provide history-enriched headers for dput",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    if base_subparsers:
        kwargs['parents'] = base_subparsers
    if subparsers:
        parser = subparsers.add_parser('prepare-upload', **kwargs)
    else:
        parser = argparse.ArgumentParser(**kwargs)

    _add_subparser_arguments(parser)
    level2_subparsers = parser.add_subparsers(required=True)
    printargs_subparser = level2_subparsers.add_parser(
        'args',
        description="Push branch and print history-enriched header arguments",
    )
    printargs_subparser.set_defaults(func=cli_printargs)
    _add_subparser_arguments(printargs_subparser)
    printargs_subparser.add_argument(
        '--output-format',
        choices=OUTPUT_FORMATS.keys(),
        default='dpkg-buildpackage',
        help="Output format",
    )
    manglechanges_subparser = level2_subparsers.add_parser(
        'mangle',
        description="Push branch and adjust history-enriched headers in changes file",
    )
    manglechanges_subparser.set_defaults(func=cli_manglechanges)
    _add_subparser_arguments(manglechanges_subparser)
    manglechanges_subparser.add_argument('changes_file_path')
    if not subparsers:
        return parser.parse_args()
    return "prepare-upload - %s" % kwargs['description']


class Parameters(collections.namedtuple('Parameters', [
    'vcs_git',
    'vcs_git_ref',
    'vcs_git_commit',
    'commit',
    'remote_name',
    'local_ref_name',
    'local_tracking_ref_name',
])):
    """The parameters associated with a prepare-upload request

    When preparing an upload, we need to know what the final Vcs-Git* headers
    in the changes file will be, as well as various other parameters such as
    the name of the configured remote that corresponds to these, the pygit2
    Commit object and so forth. These are determined once and then stored in an
    instance of this class, with the individual parameters specified here.

    :ivar str vcs_git: the contents of the eventual Vcs-Git header
    :ivar str vcs_git_ref: the contents of the eventual Vcs-Git-Ref header
    :ivar str vcs_git_commit: the contents of the eventual Vcs-Git-Commit
        header
    :ivar pygit2.Commit commit: the local commit being supplied as rich history
    :ivar str remote_name: the name of the configured remote where the rich
        history will be staged
    :ivar str local_ref_name: the ref name of the local branch that contains
        the rich history, such as "refs/heads/feature".
    :ivar str local_tracking_ref_name: the ref name of the local remote
        tracking branch that tracks the remote branch where the rich history
        will be staged, such as "refs/remotes/dave/feature".
    """
    pass

    @property
    def changes_file_headers(self):
        """The changes file headers as a dict keyed on header name

        The header names are converted from the snake_case attributes to
        deb822-case used in changes files.
        """
        return {
            k: getattr(self, k.lower().replace('-', '_'))
            for k in ['Vcs-Git', 'Vcs-Git-Ref', 'Vcs-Git-Commit']
        }

    @classmethod
    def from_repo(cls, repo, remote_name, branch_name):
        """Instantiate Parameters from user-provided string args and defaults

        This method encapsulates the heuristics used to determine what to do
        based the state of the repository and anything explicitly specified by
        the user.

        :param GitUbuntuRepository repo: the repository to consider.
        :param str remote_name: the remote name explicitly requested by the
            user, or None.
        :param str branch_name: the branch name explicitly requested by the
            user, or None.
        :rtype: Parameters
        :returns: a populated instance of this Parameters class.
        """
        if not remote_name:
            remote_name = repo.lp_user

        if branch_name:
            # XXX to identify local_tracking_ref_name, this much be a branch, not just any ref-ish
            commit, ref = repo.raw_repo.resolve_refish(branch_name)
            # ref might be None if the given branch parameter isn't actually a
            # branch (eg. is a commit)
            assert ref
        else:
            assert not repo.raw_repo.head_is_detached
            ref = repo.raw_repo.head
            assert ref.name.startswith('refs/heads/')
            branch_name = ref.shorthand
            commit = ref.peel(pygit2.Commit)

        pygit2_remote = repo.raw_repo.remotes[remote_name]

        # Rewrite rule for VCS URLs. See LP: #1942985
        vcs_git = URL_REWRITE[0].sub(URL_REWRITE[1], pygit2_remote.url)

        vcs_git_ref = ref.name
        vcs_git_commit = str(commit.id)

        # Pre-empt any failure with later input validation of the headers we
        # will generate after we push. If these assertions fail, then the later
        # import will refuse to accept this rich history, so better that we
        # fail now so the uploader knows there's some kind of problem, rather
        # than later when the importer will silently throw away the rich
        # history that the uploader pushed.
        #
        # However, we don't know of any actual case when this might happen, so
        # these are assertions rather than fully UX-compliant error paths.
        assert gitubuntu.importer.VCS_GIT_URL_VALIDATION.fullmatch(vcs_git)
        assert vcs_git.startswith(
            gitubuntu.importer.LAUNCHPAD_GIT_HOSTING_URL_PREFIX,
        )
        assert gitubuntu.importer.VCS_GIT_REF_VALIDATION.fullmatch(vcs_git_ref)
        assert gitubuntu.importer.VCS_GIT_COMMIT_VALIDATION.fullmatch(
            vcs_git_commit,
        )
        return cls(
            vcs_git=vcs_git,
            vcs_git_ref=vcs_git_ref,
            vcs_git_commit=vcs_git_commit,
            commit=commit,
            remote_name=remote_name,
            local_ref_name=ref.name,
            local_tracking_ref_name=
                'refs/remotes/%s/%s' % (remote_name, branch_name),
        )


def fetch(repo, remote_name, check=True, verbose_on_failure=True):
    """Fetch from a remote using its default refspec

    :param GitUbuntuRepository repo: the local repository to fetch into.
    :param str remote_name: the name of the remote to fetch from.
    :param bool check: passed through to GitUbuntuRepository.git_run().
    :param bool verbose_on_failure: passed through to
        GitUbuntuRepository.git_run().
    :raises subprocess.CalledProcessError: if the git call fails.
    """
    # pygit2's fetch method seems to demand authentication even though none is
    # needed in the common case that ssh keys are in use, so we call the CLI
    # instead
    repo.git_run(
        ['fetch', remote_name],
        stdout=sys.stderr,  # we do want the user to see this output, but
                            # cannot send it to stdout as that would interfere
                            # with the output of the push-for-upload command
                            # itself that needs to be captured by the user to
                            # pass to their build command.
        stderr=None,  # we do want the user to see this output
        # If the remote repository does not exist (eg. it will be created by
        # the push) then Launchpad will prompt to try authentication, but that
        # will have no meaning for the user, so we don't want a prompt and
        # instead the fetch call should fail.
        env={'GIT_TERMINAL_PROMPT': '0'},
        check=check,
        verbose_on_failure=verbose_on_failure,
    )


def push(
    repo,
    remote_name,
    local_ref_name,
    force_push=False,
):
    """Push a ref to a remote

    :param GitUbuntuRepository repo: the local repository to push from
    :param str remote_name: the name of the remote to push to.
    :param str local_ref_name: the name of the local ref to push. The same ref
        will be pushed to the remote end. Example: "refs/heads/feature".
    :param bool force_push: if a force push should be used
    :raises subprocess.CalledProcessError: if the git call fails.
    """
    # pygit2's push method seems to demand authentication even though none is
    # needed in the common case that ssh keys are in use, so we call the CLI
    # instead
    args = ['push', remote_name]
    if force_push:
        args.append('--force')
    args.append('%s:%s' % (local_ref_name, local_ref_name))
    repo.git_run(
        args,
        stdout=sys.stderr,  # we do want the user to see this output, but
                            # cannot send it to stdout as that would interfere
                            # with the output of the push-for-upload command
                            # itself that needs to be captured by the user to
                            # pass to their build command.
        stderr=None,  # we do want the user to see this output
    )


def ref_has_commit(repo, ref, commit):
    """Determine if a commit can be reached from a given reference

    :param GitUbuntuRepository repo: the repository to consider
    :param pygit2.Reference ref: which reference to look in
    :param pygit2.Commit commit: which commit to find
    :rtype: bool
    :returns: True if the commit can be reached; False otherwise

    This differs from pygit2.Repository.descendant_of() because that method
    would return False for the commit the ref points to.
    """
    ref_tip = ref.peel(pygit2.Commit)
    return (
        ref_tip.id == commit.id
        or repo.raw_repo.descendant_of(ref_tip.id, commit.id)
    )


def mod_changes_file(changes_file_path, replacements):
    """Modify an existing changes file

    :param str changes_file_path: the changes file to modify
    :param dict(str, str) replacements: the replacement keys and values
    """
    modded_changes_file_path = f'{changes_file_path}_modded'

    with open(changes_file_path) as f:
        changes = debian.deb822.Changes(f)
    changes.update(replacements)
    with open(modded_changes_file_path, 'wb') as f:
        changes.dump(f)
    os.rename(modded_changes_file_path, changes_file_path)


def ensure_in_remote(
    repo,
    remote_name,
    local_ref_name,
    local_tracking_ref_name,
    commit,
    force_push=False,
):
    """Ensure that a local ref and commit are present in a remote

    Fetch the default refspec(s) from a remote, check if a remote tracking
    branch contains a commit, and push a local ref if it does not.

    :param GitUbuntuRepository repo: the git repository to use
    :param str remote_name: the name of the remote
    :param str local_ref_name: the name of the local ref
    :param str local_tracking_ref_name: the name of local tracking branch of
        the remote ref
    :param pygit2.Commit commit: the commit
    :param bool force_push: if a force push should be used
    :raises ForcePushRequired: if a force push is required but not requested
    """
    # Do a best-effort attempt to get the remote tracking branch updated. If
    # this doesn't succeed, then we will just proceed as if our understanding
    # of the state of the remote branch is up-to-date. Therefore we can ignore
    # failures. It's possible that the user has force-pushed the remote branch
    # backwards and not updated the remote tracking branch and the fetch also
    # fails. In this case, we'll think we don't need to push, and
    # prepare-upload will supply rich history that is not present in the
    # remote. This seems unlikely and an acceptable risk.
    fetch(repo, remote_name, check=False, verbose_on_failure=False)
    try:
        local_tracking_ref = repo.raw_repo.lookup_reference(
            local_tracking_ref_name
        )
    except KeyError:
        needs_push = True
    else:
        needs_push = not ref_has_commit(repo, local_tracking_ref, commit)

        if needs_push:
            local_ref = repo.raw_repo.lookup_reference(local_ref_name)
            local_ref_commit_id = local_ref.peel(pygit2.Commit).id
            local_tracking_ref_id = local_tracking_ref.peel(pygit2.Commit).id

            # If these were the same, then a push wouldn't be required
            assert local_ref_commit_id != local_tracking_ref_id

            is_fast_forward = repo.raw_repo.descendant_of(
                local_ref_commit_id,
                local_tracking_ref_id,
            )
            if not is_fast_forward and not force_push:
                raise ForcePushRequired()

    if needs_push:
        push(
            repo=repo,
            remote_name=remote_name,
            local_ref_name=local_ref_name,
            force_push=force_push,
        )


def mangle_changes(
    repo,
    changes_file_path,
    remote_name=None,
    branch_name=None,
    force_push=False,
):
    """Pythonic API entry point to the "prepare-upload mangle" subcommand

    Ensure rich history is present in a remote for adoption by the importer,
    then mangle a changes file with the required headers.

    Replace the changes file at the given path with one that has the given
    headers overridden. If the changes file was previously signed, the
    signature will be removed.

    To avoid the risk of ending up with corrupted changes files, this writes to
    a new file and then does an atomic rename over the old path.

    :param GitUbuntuRepository repo: the git repository to use
    :param str changes_file_path: path to the changes file to mangle
    :param str remote_name: the name of the remote to use, or None to use the
        remote named after the user's Launchpad username (as added by "git
        clone").
    :param str branch_name: the name of the branch to use (both local and
        remote), or None for the name of the currently checked-out branch.
    :param bool force_push: if a force push should be used
    :returns: None
    :raises ForcePushRequired: if a force push is required but not requested
    """
    parameters = Parameters.from_repo(
        repo=repo,
        remote_name=remote_name,
        branch_name=branch_name,
    )
    ensure_in_remote(
        repo=repo,
        remote_name=parameters.remote_name,
        local_ref_name=parameters.local_ref_name,
        local_tracking_ref_name=parameters.local_tracking_ref_name,
        commit=parameters.commit,
        force_push=force_push,
    )
    mod_changes_file(
        changes_file_path=changes_file_path,
        replacements=parameters.changes_file_headers,
    )


def establish_args(repo, remote_name=None, branch_name=None, force_push=False):
    """Pythonic API entry point to the "prepare-upload args" subcommand

    Ensure rich history is present in a remote for adoption by the importer,
    then return the parameters that contain the arguments required to arrange
    for a changes file to be generated with the required headers.

    :param GitUbuntuRepository repo: the git repository to use
    :param str remote_name: the name of the remote to use, or None to use the
        remote named after the user's Launchpad username (as added by "git
        clone").
    :param str branch_name: the name of the branch to use (both local and
        remote), or None for the name of the currently checked-out branch.
    :param bool force_push: if a force push should be used
    :rtype: Parameters
    :returns: parameters that were used as determined by heuristics and the
        user
    :raises ForcePushRequired: if a force push is required but not requested
    """
    parameters = Parameters.from_repo(
        repo=repo,
        remote_name=remote_name,
        branch_name=branch_name,
    )
    ensure_in_remote(
        repo=repo,
        remote_name=parameters.remote_name,
        local_ref_name=parameters.local_ref_name,
        local_tracking_ref_name=parameters.local_tracking_ref_name,
        commit=parameters.commit,
        force_push=force_push,
    )
    return parameters


def cli_printargs(args):  # pragma: no cover
    """CLI entry point for the args subcommand"""
    try:
        repo = gitubuntu.git_repository.GitUbuntuRepository('.')
        parameters = establish_args(
            repo=repo,
            remote_name=args.remote,
            branch_name=args.branch,
            force_push=args.force_push,
        )
    except ForcePushRequired:
        print("--git-ubuntu-prepare-upload-args-failed")
        print(
            "git-ubuntu: the remote branch cannot be fast-forwarded."
                " Do you need --force-push?",
            file=sys.stderr,
        )
        return 2
    except:
        print("--git-ubuntu-prepare-upload-args-failed")
        raise
    else:
        print(
            OUTPUT_FORMATS[args.output_format].format(
                **parameters.changes_file_headers
            )
        )
        return 0


def cli_manglechanges(args):  # pragma: no cover
    """CLI entry point for the mangle subcommand"""
    repo = gitubuntu.git_repository.GitUbuntuRepository('.')
    try:
        mangle_changes(
            repo=repo,
            remote_name=args.remote,
            branch_name=args.branch,
            changes_file_path=args.changes_file_path,
            force_push=args.force_push,
        )
    except ForcePushRequired:
        print(
            "git-ubuntu: the remote branch cannot be fast-forwarded."
                " Do you need --force-push?",
            file=sys.stderr,
        )
        return 2
