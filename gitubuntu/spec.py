# This file contains constant strings that form part of the importer
# specification. Changing these strings will mutate commit hashes so must not
# be changed without careful consideration and declaration of a hash ABI break.

# Changing any importer code or code called from the importer may also cause an
# ABI break of course, but changing these constants certainly will and so are
# placed here to try to help avoid accidents.

SYNTHESIZED_COMMITTER_NAME = 'git-ubuntu importer'
SYNTHESIZED_COMMITTER_EMAIL = 'ubuntu-devel-discuss@lists.ubuntu.com'

# The name to use for a commit author when the corresponding changelog entry
# has none, since git won't accept an empty name.
EMPTY_GIT_AUTHOR_NAME = 'Unnamed'
