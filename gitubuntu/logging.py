import sys


def warning(m, *args):
    print('W:', m % args)

def error(m, *args):
    print('E:', m % args)

def fatal(m, *args):
    print('F:', m % args)
    sys.exit(2)
