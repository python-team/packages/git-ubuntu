# Requirements from Barcelona sprint with Ubuntu Server Team:
# Run with "importer-command <package>"
# Needs access to:
# 	Launchpad git push to ~git-ubuntu-import, unless --no-push is
# 	specified
#	Local git cache (maybe optional for a prototype).
# Must be idempotent.
# Must pick up any (zero or more) intermediate uploads since the previous
# import. Use Launchpad publishing history perhaps?
# Import new Debian versions automatically
#	Verification: second changelog entry must match version tag in debian
#	tracking branch.
# Import new Ubuntu versions automatically
#	Verification: second changelog entry must match version tag in current
#	Ubuntu tracking branch, OR match a newer parent version tag in debian
#	tracking branch.
#	Import will use the verified parent as the parent commit.
# If verification fails, then import anyway, but log and use an orphan commit.
#
# Results in a set of branches (git heads):
#    - One per distribution/series/pocket
#      + note in LP web UI that series is called target
#    - For a given import, the parent(s) of that commit are:
#       + the previous published version in distribution/series/pocket
#       + the last imported version from debian/changelog

import argparse
import datetime
import functools
import getpass
import logging
import os
import re
import shutil
from subprocess import CalledProcessError
import sys
import tempfile
import time
import traceback
import urllib.request

import debian.deb822
import tenacity

from gitubuntu.__main__ import top_level_defaults
from gitubuntu.cache import CACHE_PATH
from gitubuntu.dsc import (
    GitUbuntuDsc,
    GitUbuntuDscError,
)
from gitubuntu.git_repository import (
    GitUbuntuRepository,
    GitUbuntuRepositoryFetchError,
    orphan_tag,
    import_tag,
    reimport_tag,
    upstream_tag,
    PristineTarError,
    is_dir_3_0_quilt,
)
from gitubuntu.patch_state import PatchState
import gitubuntu.rich_history as rich_history
from gitubuntu.run import (
    decode_binary,
    run,
    runq,
    run_gbp,
    run_quilt,
)
from gitubuntu.source_information import (
    GitUbuntuSourceInformation,
    NoPublicationHistoryException,
    SourceExtractionException,
    launchpad_login_auth,
)
import gitubuntu.spec as spec
from gitubuntu.version import VERSION
from gitubuntu.versioning import version_compare
import pygit2
from lazr.restfulclient.errors import NotFound, PreconditionFailed


# These constants are essential for validation of untrusted input to avoid the
# risk of arbitrary code execution when we call git to fetch rich history using
# uploader-submitted information. For details, see the docstrings of the
# functions where they are used below.
LAUNCHPAD_GIT_HOSTING_URL_PREFIX = "https://git.launchpad.net/"
VCS_GIT_URL_VALIDATION = re.compile(
    r'https://[A-Za-z0-9_.-]+(/[A-Za-z0-9._~/?=&+-]*)?',
)
VCS_GIT_REF_VALIDATION = re.compile(r'refs/[A-Za-z0-9./+_-]+')
VCS_GIT_COMMIT_VALIDATION = re.compile(r'[A-Za-z0-9]{40}')


# These are the first bytes in stderr from git if "git fetch" is called and the
# remote doesn't have the ref requested. This seems to be the "least worst" way
# of treating such a failure as a hard failure, rather than other fetch
# failures which are treated as soft failures.
#
# There is a test that uses this constant to ensure that this undocumented git
# behaviour doesn't change. This is a translatable string inside git, so git
# must be called with LC_ALL="C.UTF-8".
GIT_REMOTE_REF_MISSING_PREFIX = b"fatal: couldn't find remote ref"

# These are the first bytes in stderr from git if "git fetch" is called and
# prompts for authentication but we have disabled that with
# GIT_TERMINAL_PROMPT=0. This seems like the least worst method of detecting
# this case to turn it into a RichHistoryNotFound exception.
GIT_REMOTE_AUTH_FAILED_PREFIX = b"fatal: could not read Username "


class GitUbuntuImportError(Exception):
    pass


class GitUbuntuImportOrigError(GitUbuntuImportError):
    pass


class ParentOverrideError(GitUbuntuImportError):
    pass


def dsc_to_tree_hash(pygit2_repo, dsc_path, patches_applied=False):
    '''Convert a dsc file into a git tree in the given repo

    pygit2_repo: pygit2.Repository object
    dsc_path: string path to dsc file
    patches_applied: bool whether patches should be applied on
        extraction

    Returns: tree hash as a hex string
    '''
    with tempfile.TemporaryDirectory() as temp_dir:
        # dpkg-source -x <dsc> <output_dir> requires output_dir to not
        # exist. So we use temp_dir to contain the real output_dir, which
        # we call "x". We store the whole path, including the "x", in
        # extracted_dir.

        extracted_dir = os.path.join(temp_dir, 'x')
        cmd = ['dpkg-source', '-x']
        if not patches_applied:
            cmd.append('--skip-patches')
        # extracted_dir is <temp_dir>/x
        cmd.extend([dsc_path, extracted_dir])
        run(cmd)

        if not os.path.exists(extracted_dir):
            logging.error("No source extracted?")
            raise SourceExtractionException(
                "Failed to find an extracted directory from "
                "dpkg-source -x"
            )

        return GitUbuntuRepository.dir_to_tree(
            pygit2_repo,
            extracted_dir,
            escape=True,
        )


# XXX: need a namedtuple to hold common arguments
def _main_with_repo(
    repo,
    pkgname,
    owner,
    no_clean=False,
    no_fetch=False,
    push=True,
    active_series_only=False,
    skip_orig=False,
    skip_applied=False,
    reimport=False,
    allow_applied_failures=False,
    directory=None,
    dl_cache=None,
    user=None,
    proto=top_level_defaults.proto,
    pullfile=top_level_defaults.pullfile,
    parentfile=top_level_defaults.parentfile,
    retries=top_level_defaults.retries,
    retry_backoffs=top_level_defaults.retry_backoffs,
    rich_history_tmpdir=None,
    changelog_date_override_file=None,
    set_as_default_repository=False,
):
    """Internal main function of the importer

    Arguments:
    @repo: GitUbuntuRepository to import to. Unlike the main function, this
    object will not be closed when this function returns.

    For the meaning of all other arguments, see the docstring of the main
    function.

    Returns 0 on successful import (which includes non-fatal failures);
    1 otherwise.
    """
    if owner == 'git-ubuntu-import':
        namespace = 'importer'
    else:
        namespace = owner

    repo.add_base_remotes(pkgname, repo_owner=owner)
    if not no_fetch and not reimport:
        try:
            repo.fetch_base_remotes()
        except GitUbuntuRepositoryFetchError:
            pass

    if not no_fetch:
        repo.delete_branches_in_namespace(namespace)
        repo.delete_tags_in_namespace(namespace)
        repo.copy_base_references(namespace)

    if not no_fetch and not reimport:
        try:
            # This is a normal import run. Fetch in to the "namespaced" local
            # refs the tags and changelog notes (the branch fetches are handled
            # elsewhere). The changelog notes are kept at refs/notes/commits on
            # Launchpad due to LP: #1871838 even though our standard place for
            # them is refs/notes/changelog.
            repo.fetch_remote_refspecs('pkg',
                refspecs=[
                    'refs/tags/*:refs/tags/%s/*' % namespace,
                    'refs/notes/commits:refs/notes/%s/changelog' % namespace,
                    'refs/notes/importer:refs/notes/%s/importer' % namespace,
                ],
            )
        except GitUbuntuRepositoryFetchError:
            pass

    if reimport:
        if no_fetch:
            logging.warning(
                "--reimport specified with --no-fetch. Upload "
                "tags would not be incorporated into the new import "
                "and would be lost forever. This is not currently "
                "supported and --no-fetch will be ignored for upload "
                "tags."
            )
        # Fetch previous import and upload tags for rich history export
        repo.fetch_remote_refspecs('pkg',
            refspecs=[
                'refs/tags/upload/*:refs/tags/%s/upload/*' % namespace,
                'refs/tags/import/*:refs/tags/%s/import/*' % namespace,
            ],
        )
        rich_history.export_all(
            repo=repo,
            path=rich_history_tmpdir,
            namespace=namespace,
        )
        # Delete previous tags for reimport
        repo.delete_tags_in_namespace(namespace)
        # Delete previous notes for reimport
        for ref in repo.references_with_prefix('refs/notes/%s/' % (namespace)):
            ref.delete()

    debian_sinfo = GitUbuntuSourceInformation(
        'debian',
        pkgname,
        os.path.abspath(pullfile),
        retries,
        retry_backoffs,
    )

    ubuntu_sinfo = GitUbuntuSourceInformation(
        'ubuntu',
        pkgname,
        os.path.abspath(pullfile),
        retries,
        retry_backoffs,
    )

    debian_head_info = (
        repo.get_head_info('debian', namespace)
    )
    for head_name in sorted(debian_head_info):
        _, _, pretty_head_name = head_name.partition('%s/' % namespace)
        logging.debug('Last imported %s version is %s',
            pretty_head_name,
            debian_head_info[head_name].version
        )

    ubuntu_head_info = (
        repo.get_head_info('ubuntu', namespace)
    )
    for head_name in sorted(ubuntu_head_info):
        _, _, pretty_head_name = head_name.partition('%s/' % namespace)
        logging.debug('Last imported %s version is %s',
            pretty_head_name,
            ubuntu_head_info[head_name].version
        )

    if not skip_applied:
        applied_debian_head_info = (
            repo.get_head_info(
                'applied/debian', namespace
            )
        )
        for head_name in sorted(applied_debian_head_info):
            _, _, pretty_head_name = head_name.partition(
                '%s/' % namespace
            )
            logging.debug('Last applied %s version is %s',
                pretty_head_name,
                applied_debian_head_info[head_name].version
            )

        applied_ubuntu_head_info = (
            repo.get_head_info(
                'applied/ubuntu', namespace
            )
        )
        for head_name in sorted(applied_ubuntu_head_info):
            _, _, pretty_head_name = head_name.partition(
                '%s/' % namespace
            )
            logging.debug('Last applied %s version is %s',
                pretty_head_name,
                applied_ubuntu_head_info[head_name].version
            )

    oldcwd = os.getcwd()
    os.chdir(repo.local_dir)

    if dl_cache is None:
        workdir = os.path.join(repo.git_dir, CACHE_PATH)
    else:
        workdir = dl_cache

    os.makedirs(workdir, exist_ok=True)

    parent_overrides = parse_parentfile(parentfile, pkgname)
    changelog_date_overrides = frozenset(parse_changelog_date_overrides(
        changelog_date_override_file,
        pkgname,
    ))

    history_found = import_publishes(
        repo=repo,
        pkgname=pkgname,
        namespace=namespace,
        patches_applied=False,
        debian_head_info=debian_head_info,
        ubuntu_head_info=ubuntu_head_info,
        debian_sinfo=debian_sinfo,
        ubuntu_sinfo=ubuntu_sinfo,
        active_series_only=active_series_only,
        workdir=workdir,
        skip_orig=skip_orig,
        allow_applied_failures=allow_applied_failures,
        parent_overrides=parent_overrides,
        rich_history_tmpdir=rich_history_tmpdir,
        changelog_date_overrides=changelog_date_overrides,
    )

    if not history_found:
        logging.error("No publication history for '%s' in debian or ubuntu. "
                      "Wrong source package name?", pkgname)
        return 1

    if not skip_applied:
        import_publishes(
            repo=repo,
            pkgname=pkgname,
            namespace=namespace,
            patches_applied=True,
            debian_head_info=applied_debian_head_info,
            ubuntu_head_info=applied_ubuntu_head_info,
            debian_sinfo=debian_sinfo,
            ubuntu_sinfo=ubuntu_sinfo,
            active_series_only=active_series_only,
            workdir=workdir,
            skip_orig=skip_orig,
            allow_applied_failures=allow_applied_failures,
            parent_overrides=parent_overrides,
        )

    update_devel_branches(
        repo=repo,
        namespace=namespace,
        pkgname=pkgname,
        ubuntu_sinfo=ubuntu_sinfo,
    )

    os.chdir(oldcwd)

    repo.garbage_collect()

    if not push:
        logging.info('Not pushing to remote as specified')
        return 0

    lp = launchpad_login_auth()
    repo_path = '~%s/ubuntu/+source/%s/+git/%s' % (
        owner,
        pkgname,
        pkgname,
    )

    # If reimporting, then "--force --prune" is sufficient to replace all
    # refs with the new ones, as the refspecs provided here is the complete
    # set that we generate, and are only pushed from this call. Upload tags
    # during rich history preservation are generated during the reimport so
    # are also included.
    repo.git_run(
        [
            'push', '--atomic',
        ] + (
            ['--force', '--prune'] if reimport else []
        ) + [
            'pkg',
            '+refs/heads/%s/*:refs/heads/*' % namespace,
            'refs/tags/%s/*:refs/tags/*' % namespace,
            # Changelog notes go to refs/notes/commits due to LP: #1871838
            'refs/notes/%s/changelog:refs/notes/commits' % namespace,
            'refs/notes/%s/importer:refs/notes/importer' % namespace,
        ]
    )

    lp_git_repo = lp.git_repositories.getByPath(path=repo_path)

    # Update HEAD on LP to point to our desired default.
    # If there is an existing reference to a '*/ubuntu/devel' branch
    # within a namespace, use ubuntu/devel. Otherwise, default
    # to debian/sid.
    ubuntu_devel_ref_name = 'refs/heads/%s/ubuntu/devel' % namespace
    try:
        repo.raw_repo.lookup_reference(ubuntu_devel_ref_name)
    except KeyError:
        desired_lp_default_branch = 'refs/heads/debian/sid'
    else:
        desired_lp_default_branch = 'refs/heads/ubuntu/devel'
    for i in range(retries):
        try:
            logging.debug('Setting LP HEAD to %s' % desired_lp_default_branch)
            lp_git_repo.default_branch = desired_lp_default_branch
            lp_git_repo.lp_save()
            break
        except (NotFound, PreconditionFailed) as e:
            time.sleep(retry_backoffs[i])
            lp_git_repo.lp_refresh()

    if set_as_default_repository:
        logging.debug('Setting repository as default for its target')
        lp.git_repositories.setDefaultRepository(
            repository=lp_git_repo,
            target=lp_git_repo.target,
        )

    return 0


# XXX: need a namedtuple to hold common arguments
def main(
    pkgname,
    owner,
    no_clean=False,
    no_fetch=False,
    push=True,
    active_series_only=False,
    skip_orig=False,
    skip_applied=False,
    reimport=False,
    allow_applied_failures=False,
    directory=None,
    dl_cache=None,
    user=None,
    proto=top_level_defaults.proto,
    pullfile=top_level_defaults.pullfile,
    parentfile=top_level_defaults.parentfile,
    retries=top_level_defaults.retries,
    retry_backoffs=top_level_defaults.retry_backoffs,
    repo=None,
    changelog_date_override_file=None,
    set_as_default_repository=False,
):
    """Main entry point to the importer

    Arguments:
    @pkgname: string name of source package
    @owner: string owner of repository
    @no_clean: if True, do not delete the local directory
    @no_fetch: if True, do not fetch the target repository before
    importing
    @push: push to the target repository only if True
    @active_series_only: if True, only import active series
    @skip_orig: if True, do not attempt to use gbp to import orig
    tarballs
    @skip_applied: if True, do not attempt to import patches-applied
    publishes
    @reimport: if True, import the source package from scratch and
    delete/recreate the target repository.
    @allow_applied_failures: if True, and patches fail to apply for any
    publish, that patches-applied import will be skipped rather than an
    error
    @directory: string path for local repository
    @user: string user to authenticate to Launchpad as
    @proto: string protocol to use (one of 'http', 'https', 'git')
    @dl_cache: string path where downloaded files should be cached
    @pullfile: string path to file specifying pull overrides
    @parentfile: string path to file specifying parent overrides
    @retries: integer number of download retries to attempt
    @retry_backoffs: list of backoff durations to use between retries
    @repo: if specified, a GitUbuntuRepository instance. If not specified, a
    GitUbuntuRepository instance will be created using the provided parameters.
    Regardless of whether it was specified or not, the instance will be closed
    before this function returns.
    :param str changelog_date_override_file: if specified, the path to a file
        specifying versions of packages with changelog dates that should be
        ignored. The publishing date of the source package data instance should
        be used instead. See parse_changelog_date_overrides() for the required
        format of the file.
    :param bool set_as_default_repository: if True, then after pushing, set the
        repository in Launchpad as default for its target. Does nothing if
        :py:attr:`push` is not set.

    If directory is None, a temporary directory is created and used.

    If user is None, value of `git config gitubuntu.lpuser` will be
    used.

    If dl_cache is None, CACHE_PATH in the local repository will be
    used.

    Returns 0 on successful import (which includes non-fatal failures);
    1 otherwise.
    """
    logging.info('Ubuntu Server Team importer v%s' % VERSION)

    if not repo:
        repo = GitUbuntuRepository(
            directory,
            user,
            proto,
            delete_on_close=not no_clean,
        )
    if not directory:
        logging.info('Using git repository at %s', repo.local_dir)

    try:
        with tempfile.TemporaryDirectory() as rich_history_tmpdir:
            _main_with_repo(
                repo=repo,
                pkgname=pkgname,
                owner=owner,
                no_clean=no_clean,
                no_fetch=no_fetch,
                push=push,
                active_series_only=active_series_only,
                skip_orig=skip_orig,
                skip_applied=skip_applied,
                reimport=reimport,
                allow_applied_failures=allow_applied_failures,
                directory=directory,
                user=user,
                dl_cache=dl_cache,
                proto=proto,
                pullfile=pullfile,
                parentfile=parentfile,
                retries=retries,
                retry_backoffs=retry_backoffs,
                rich_history_tmpdir=rich_history_tmpdir,
                changelog_date_override_file=changelog_date_override_file,
                set_as_default_repository=set_as_default_repository,
            )
    finally:
        repo.close()

def get_changelog_for_commit(
    repo,
    tree_hash,
    changelog_parent_commits,
):
    """Extract changes to debian/changelog in this publish

    LP: #1633114 -- favor the changelog parent's diff
    """
    raw_clog_entry = b''
    # If there is more than one changelog parent then don't attempt to combine
    # their entries, and instead return nothing (raw_clog_entry will remain
    # b''). This can be addressed later when the specification better defines
    # what the commit message should say in this case, but will do for now.
    if len(changelog_parent_commits) == 1:
        cmd = ['diff-tree', '-p', changelog_parent_commits[0],
               tree_hash, '--', 'debian/changelog']
        raw_clog_entry, _ = repo.git_run(cmd, decode=False)

    changelog_entry = b''
    changelog_entry_found = False
    i = 0
    for line in raw_clog_entry.split(b'\n'):
        # skip the header lines
        if i < 4:
            i += 1
            continue
        if not line.startswith(b'+'):
            # in case there are stray changelog changes, just take
            # the first complete section of changelog additions
            if changelog_entry_found:
                break
            continue
        line = re.sub(b'^[+]', b'', line)
        if not line.startswith(b'  '):
            continue
        changelog_entry += line + b'\n'
        changelog_entry_found = True
    return changelog_entry

def get_import_commit_msg(
    repo,
    version,
    patch_state,
):
    """Compose an appropriate commit message

    When the importer synthesizes a commit, it needs to provide an appropriate
    commit message. This function centralizes the generation of that message
    based on the parameters given.

    :param GitUbuntuRepository repo: the repository in which the tree being
        committed can be found
    :param str version: the package version string being committed
    :param PatchState patch_state: whether the commit is for an unapplied or
        applied import
    :rtype: bytes
    :returns: the commit message to use
    """
    import_type = {
        PatchState.UNAPPLIED: 'patches unapplied',
        PatchState.APPLIED: 'patches applied',
    }[patch_state]

    return (
        b'%b (%b)\n\nImported using git-ubuntu import.' % (
            version.encode(),
            import_type.encode(),
        )
    )


def _commit_import(
    repo,
    version,
    tree_hash,
    namespace,
    changelog_parent_commits,
    unapplied_parent_commit,
    commit_date,
    author_date=None,
):
    """Commit a tree object into the repository with the specified
    parents

    The importer algorithm works by 'staging' the import of a
    publication as a tree, then using that tree to determine the
    appropriate parents from the publishing history and
    debian/changelog.

    :param repo gitubuntu.GitUbuntuRepository The Git repository into
         which the source package contents corresponding to @spi should
         be imported.
    :param version str Changelog version
    :param namespace str Namespace under which relevant refs can be
         found.
    :param list(str) changelog_parent_commits Git commit hashes of changelog
         parents.
    :param unapplied_parent_commit str Git commit hash of unapplied
         parent. Should be None if patches-unapplied import.
    :param str commit_date: committer date to use for the git commit metadata
    :param datetime.datetime author_date: overrides the author date normally
        parsed from the changelog entry (i.e. for handling date parsing edge
        cases). Any sub-second part of the timestamp is truncated.
    :rtype: pygit2.Commit
    :returns: created commit object
    """
    parents = []

    parents.extend(
        pygit2.Oid(hex=changelog_parent_commit)
        for changelog_parent_commit in changelog_parent_commits
    )
    if unapplied_parent_commit is not None:
        parents.append(pygit2.Oid(hex=unapplied_parent_commit))

    if unapplied_parent_commit:
        patch_state = PatchState.APPLIED
    else:
        patch_state = PatchState.UNAPPLIED

    commit_hash = repo.commit_source_tree(
        tree=pygit2.Oid(hex=tree_hash),
        parents=parents,
        log_message=get_import_commit_msg(
            repo=repo,
            version=version,
            patch_state=patch_state,
        ),
        commit_date=commit_date,
        author_date=author_date,
    )

    return repo.raw_repo.get(str(commit_hash))


def import_dsc(repo, dsc, namespace, version, dist):
    """Imports the dsc file to importer/{debian,ubuntu}/dsc

    Arguments:
    spi - A GitUbuntuSourcePackageInformation instance
    """

    # Add DSC file to the appropriate branch
    dsc_branch_name = '%s/importer/%s/dsc' % (namespace, dist)
    dsc_ref_name = 'refs/heads/%s' % dsc_branch_name
    try:
        repo.raw_repo.lookup_reference(dsc_ref_name)
    except KeyError:
        runq(['git', 'checkout', '--orphan', dsc_branch_name], env=repo.env)
        runq(['git', 'reset', '--hard'], env=repo.env)
    else:
        runq(['git', 'checkout', dsc_branch_name], env=repo.env)

    # It is possible the same-named DSC is published multiple times
    # with different contents, e.g. on epoch bumps
    local_dir = os.path.join(repo.local_dir, version)
    os.makedirs(local_dir, exist_ok=True)
    shutil.copy(dsc.dsc_path, local_dir)
    repo.git_run(['add', repo.local_dir])
    try:
        # Anything to commit? (this will be 0 if, for instance, the
        # same dsc is being imported again)
        runq(['git', 'diff', '--exit-code', 'HEAD'], env=repo.env)
    except CalledProcessError:
        repo.git_run(['commit', '-m', 'DSC file for %s' % version])

    repo.clean_repository_state()

def orig_imported(repo, orig_tarball_paths, namespace, dist):
    """Determines if a list of paths to tarballs have already been imported to @dist

    Assumes that the pristine-tar branch exists
    """
    try:
        return repo.verify_pristine_tar(
            orig_tarball_paths,
            dist,
            namespace,
        )
    except PristineTarError as e:
        raise GitUbuntuImportOrigError from e

def import_orig(repo, dsc, namespace, version, dist):
    """Imports the orig-tarball using gbp import-org --pristine-tar

    Arguments:
    repo - A GitUbuntuRepository object
    dsc - a GitUbuntuDsc object
    namespace - string namespace to search for tags, etc.
    version - the string package version
    dist - the string distribution, either 'ubuntu' or 'debian'
    """
    try:
        orig_tarball_path = dsc.orig_tarball_path
    except GitUbuntuDscError as e:
        raise GitUbuntuImportOrigError(
            'Unable to import orig tarball in DSC for version %s' % version
        ) from e

    if orig_tarball_path is None:
        # native packages only have a .tar.*
        native_re = re.compile(r'.*\.tar\.[^.]+$')
        for entry in dsc['Files']:
            if native_re.match(entry['name']):
                return
        raise GitUbuntuImportOrigError('No orig tarball in '
            'DSC for version %s.' % version
        )

    try:
        orig_component_paths = dsc.component_tarball_paths
    except GitUbuntuDscError as e:
        raise GitUbuntuImportOrigError(
            'Unable to import component tarballs in DSC for version '
            '%s' % version
        ) from e

    orig_paths = [orig_tarball_path] + list(orig_component_paths.values())
    if (not orig_tarball_path or
        orig_imported(repo, orig_paths, namespace, dist)
    ):
        return
    if not all(map(os.path.exists, orig_paths)):
        raise GitUbuntuImportOrigError('Unable to find tarball: '
            '%s' % [p for p in orig_paths if not os.path.exists(p)])

    try:
        with repo.pristine_tar_branches(dist, namespace):
            oldcwd = os.getcwd()
            # gbp does not support running from arbitrary git trees or
            # working directories
            # https://github.com/agx/git-buildpackage/pull/16
            os.chdir(repo.local_dir)

            ext = os.path.splitext(dsc.orig_tarball_path)[1]

            # make this non-fatal if upstream already exist as tagged?
            args = ['import-orig', '--no-merge',
                 '--upstream-branch', 'do-not-push',
                 '--pristine-tar', '--no-interactive',
                 '--no-symlink-orig',
                 '--upstream-tag=%s/upstream/%s/%%(version)s%s' %
                     (namespace, dist, ext)]
            args.extend(map(lambda x: '--component=%s' % x,
                list(orig_component_paths.keys()))
            )
            args.extend([orig_tarball_path,])
            run_gbp(args, env=repo.env)
    except CalledProcessError as e:
        raise GitUbuntuImportOrigError(
            'Unable to import tarballs: %s' % orig_paths
        ) from e
    finally:
        os.chdir(oldcwd)

        repo.clean_repository_state()


def import_patches_applied_tree(repo, dsc_pathname):
    """Imports the patches-applied source package and writes the
    corresponding working tree for a given srcpkg, patch by patch

    Arguments:
    dsc_pathname - A string path to a DSC file
    """
    oldcwd = os.getcwd()

    extract_dir = tempfile.mkdtemp()
    os.chdir(extract_dir)

    run(['dpkg-source', '-x',
         '--skip-patches',
         os.path.join(oldcwd, dsc_pathname)
        ]
       )

    extracted_dir = None
    for path in os.listdir(extract_dir):
        if os.path.isdir(path):
            extracted_dir = os.path.join(extract_dir, path)
            break
    if extracted_dir is None:
        logging.exception('No source extracted?')
        raise SourceExtractionException("Failed to find an extracted "
                                        "directory from dpkg-source -x")

    repo.git_run(
        ['--work-tree', extracted_dir, 'add', '-f', '-A']
    )

    # If .pc exists then we remove it in a separate commit so that it doesn't
    # interfere with our upcoming quilt patch applications.
    has_dot_pc = os.path.isdir(os.path.join(extracted_dir, '.pc'))
    if has_dot_pc:
        repo.git_run(
            ['--work-tree', extracted_dir, 'rm', '-r', '-f', '.pc']
        )

    # In all cases we need the tree object in order to determine the correct
    # quilt environment later. import_tree_hash will be updated to always be
    # the hex hash string of the tree object before quilt push was applied.
    import_tree_hash_ws, _ = repo.git_run(
        ['--work-tree', extracted_dir, 'write-tree']
    )
    import_tree_hash = import_tree_hash_ws.strip()

    # If .pc did exist, then we yield its tree object hash string in order for
    # the caller to create a separate commit for it.
    if has_dot_pc:
        yield (
            import_tree_hash.strip(),
            None,
            'Remove .pc directory from source package',
        )

    try:
        if not is_dir_3_0_quilt(extracted_dir):
            return

        while True:
            try:
                os.chdir(extracted_dir)
                run_quilt(
                    ['push'],
                    env=repo.quilt_env_from_treeish_str(import_tree_hash),
                    verbose_on_failure=False,
                )
                patch_name, _ = run_quilt(
                    ['top'],
                    env=repo.quilt_env_from_treeish_str(import_tree_hash),
                )
                patch_name = patch_name.strip()
                header, _  = run_quilt(
                    ['header'],
                    env=repo.quilt_env_from_treeish_str(import_tree_hash),
                )
                header = header.strip()
                patch_desc = None
                for regex in (r'Subject:\s*(.*?)$',
                              r'Description:\s*(.*?)$'
                             ):
                    try:
                        m = re.search(regex, header, re.MULTILINE)
                        patch_desc = m.group(1)
                    except AttributeError:
                        pass
                if patch_desc is None:
                    logging.debug('Unable to find Subject or Description '
                        'in %s' % patch_name
                    )
                repo.git_run([
                    '--work-tree',
                    extracted_dir,
                    'add',
                    '-f',
                    '-A',
                ])
                repo.git_run([
                    '--work-tree',
                    extracted_dir,
                    'reset',
                    'HEAD',
                    '--',
                    '.pc',
                ])

                # Update import_tree_hash both in order to be able to yield it
                # but also to make it available to determine the quilt
                # environment in the next iteration of this loop.
                import_tree_hash_ws, _ = repo.git_run(
                    ['--work-tree', extracted_dir, 'write-tree']
                )
                import_tree_hash = import_tree_hash_ws.strip()

                yield (import_tree_hash, patch_name, patch_desc)
            except CalledProcessError as e:
                # quilt returns 2 when done pushing
                if e.returncode != 2:
                    raise
                return
    finally:
        os.chdir(oldcwd)
        shutil.rmtree(extract_dir)
        repo.clean_repository_state()

def parse_parentfile(parentfile, pkgname):
    """Extract parent overrides from a file

    The parent overrides file specifies parent-child relationship(s),
    where the importer may not be able to determine them automatically.
    The format of the file is:
    <pkgname> <child version> <changelog parent version>
    with one override per line.

    <child version> is the published version to which this override
    applies.

    The <changelog parent version> is the prior version in the
    child publish's debian/changelog.

    :param str parentfile: path to parent overrides file
    :param str pkgname: source package name on which to filter
    :rtype: dict
    :returns: dictionary keyed by version string of dictionaries keyed by
        'changelog_parent' of strings of changelog parent versions.
    """
    parent_overrides = dict()
    try:
        with open(parentfile) as f:
            for line in f:
                if line.startswith('#'):
                    continue
                m = re.match(
                    r'(?P<pkgname>\S*)\s*(?P<childversion>\S*)\s*(?P<parent2version>\S*)',
                    line
                    )
                if m is None:
                    continue
                if m.group('pkgname') != pkgname:
                    continue
                parent_overrides[m.group('childversion')] = {
                    'changelog_parent': m.group('parent2version')
                }
    except FileNotFoundError:
        pass

    return parent_overrides


def parse_changelog_date_overrides(file_path, package):
    """Extract changelog date overrides from a file

    Given a changelog date override file path and a package name, return the
    version strings whose changelog dates are to be overridden for that
    package.

    When a changelog date is overridden, the timestamp of the source package
    data instance is used for the author timestamp metadata in a synthesized
    commit, same as the commit timestamp metadata. This is only to be done when
    explicitly listed in the specification and is used to work around invalid
    dates in changelog files. See the specification for details.

    File format: space-separated lines, where each line contains a package name
    and version string. Extra whitespace is ignored. Any line starting with a
    '#' is ignored.

    If file_path is None, then an empty sequence is returned.

    :param str file_path: the path to the changelog date overrides file
    :param str package: the name of the package for which to report overrides
    :rtype: sequence(str)
    :returns: version strings whose changelog dates are to be overridden
    """
    if not file_path:
        return
    with open(file_path) as fobj:
        for line in fobj:
            stripped_line = line.strip()
            if not stripped_line or stripped_line.startswith('#'):
                continue
            line_package, line_version = stripped_line.split()
            if line_package == package:
                yield line_version


def get_existing_import_tags(
    repo,
    version,
    namespace,
    patch_state=PatchState.UNAPPLIED,
):
    """Find and return any import tags that already exist for the given version

    :param GitUbuntuRepository repo: repository to search for import tags
    :param str version: the package version string to find
    :param str namespace: the name to prefix to the import tag names searched
    :param PatchState patch_state: whether to look for unapplied or applied
        tags
    :rtype: list(pygit2.Reference)
    :returns: a list of import tags found. If reimport tags exist, this is the
        list of matching reimport tags in order; the original import tag is not
        included as it is already represented by the first reimport tag. If
        reimport tags do not exist, just the sole import tag is returned. If
        none of these are found, the empty list is returned.
    """
    existing_import_tag = repo.get_import_tag(version, namespace, patch_state)
    if existing_import_tag:
        # check if there are reimports; if no import tag exists, then by the
        # specification there can be no reimports
        existing_reimport_tags = repo.get_all_reimport_tags(
            version,
            namespace,
            patch_state,
        )
        if existing_reimport_tags:
            return sorted(
                existing_reimport_tags,
                # Sort on the integer of the last '/'-separated component
                key=lambda tag: int(tag.name.split('/')[-1]),
            )
        else:
            return [existing_import_tag]
    return []


def override_parents(parent_overrides, repo, version, namespace):
    """
    returns: (unapplied, applied) where both elements of the tuple are lists of
        commit hash strings of the changelog parents to use, incorporating any
        defined overrides.
    rtype: tuple(list(str), list(str))
    """
    unapplied_changelog_parent_commit = None
    applied_changelog_parent_commit = None

    unapplied_changelog_parent_tag = repo.get_import_tag(
        parent_overrides[version]['changelog_parent'],
        namespace,
        PatchState.UNAPPLIED,
    )
    applied_changelog_parent_tag = repo.get_import_tag(
        parent_overrides[version]['changelog_parent'],
        namespace,
        PatchState.APPLIED,
    )
    if unapplied_changelog_parent_tag is not None:
        # coherence check that version from d/changelog of the
        # tagged commit matches ours
        parent_changelog_version, _ = repo.get_changelog_versions_from_treeish(
            str(unapplied_changelog_parent_tag.peel().id),
        )
        if parent_changelog_version != parent_overrides[version]['changelog_parent']:
            logging.error('Found a tag corresponding to '
                          'changelog parent override '
                          'version %s, but d/changelog '
                          'version (%s) differs. Will '
                          'orphan commit.' % (
                          parent_overrides[version]['changelog_parent'],
                          parent_changelog_version
                          )
                         )
        else:
            unapplied_changelog_parent_commit = str(unapplied_changelog_parent_tag.peel().id)
            if applied_changelog_parent_tag is not None:
                applied_changelog_parent_commit = str(applied_changelog_parent_tag.peel().id)
            logging.debug('Overriding changelog parent (tag) to %s',
                repo.tag_to_pretty_name(unapplied_changelog_parent_tag)
            )
    else:
        if parent_overrides[version]['changelog_parent'] == '-':
            logging.debug('Not setting changelog parent as specified '
                'in override file.'
            )
        else:
            raise ParentOverrideError(
                "Specified changelog parent override (%s) for version "
                "(%s) not found in tags. Unable to proceed." % (
                    parent_overrides[version]['changelog_parent'],
                    version,
                )
            )
    return (
        [unapplied_changelog_parent_commit],
        [applied_changelog_parent_commit],
    )

def _devel_branch_updates(
    ref_prefix,
    head_info,
    series_name_list,
):
    """Calculate devel branch pointer commits.

    For all series in series_name_list, yield a (ref_name, hash_string) pair
    of where the devel pointer for that series should point.

    Also yield a further generic 'ubuntu/devel' ref_name with its target.

    :param: ref_prefix: what to stick on the front of all refs yielded and to
        look for in head_info

    :param: head_info: a dictionary as returned by
        GitUbuntuRepository.get_head_info()

    :param: series_name_list: a list of strings of series names to consider
        with most recent series first.
    """

    devel_hash = None
    devel_name = None
    devel_branch_name = ref_prefix + 'ubuntu/devel'

    for series_name in series_name_list:
        series_devel_hash = None
        series_devel_name = None
        series_devel_version = None
        # Find highest version publish in these pockets, favoring this
        # order of pockets
        for suff in ['-proposed', '-updates', '-security', '']:
            name = "%subuntu/%s%s" % (ref_prefix, series_name, suff)
            if name in head_info:
                if version_compare(head_info[name].version, series_devel_version) > 0:
                    series_devel_name = name
                    series_devel_version = head_info[name].version
                    series_devel_hash = str(head_info[name].commit_id)
        if series_devel_hash:
            series_devel_branch_name = '%subuntu/%s-devel' % (
                ref_prefix,
                series_name,
            )
            yield series_devel_branch_name, series_devel_hash

            # the first series devel pointer we find is the most recent
            # XXX: if a source package was published, but no longer is,
            # do we want to have a ubuntu-devel pointer? This was the
            # old behavior, but we need to decide on the semantics of
            # the devel branch (most recent or development release)
            if not devel_hash:
                devel_name = series_devel_name
                devel_hash = series_devel_hash

    yield ref_prefix + 'ubuntu/devel', devel_hash


def update_devel_branches(
    repo,
    namespace,
    pkgname,
    ubuntu_sinfo,
):
    """update_devel_branches - move the 'meta' -devel branches to the
    latest publication in each series

    For all known series in @ubuntu_sinfo, update the
    ubuntu/series-devel branch to the latest version in the series'
    pocket branches.

    Update the ubuntu/devel branch to the latest ubuntu/series-devel
    branch.

    Do this for both the unapplied and applied branches.

    Arguments:
    repo - gitubuntu.git_repository.GitUbuntuRepository object
    pkgname - string source package name
    namespace - string namespace under which the relevant branch refs
    can be found
    ubuntu_sinfo - GitUbuntuSourceInformation object for the ubuntu
    archive
    """
    for applied_prefix in ['', 'applied/']:
        for ref, commit in _devel_branch_updates(
            ref_prefix=('%s/%s' % (namespace, applied_prefix)),
            head_info=repo.get_head_info(
                head_prefix='%subuntu' % applied_prefix,
                namespace=namespace,
            ),
            series_name_list=ubuntu_sinfo.all_series_name_list,
        ):
            if commit is None:
                logging.warning(
                    "Source package '%s' does not appear to have been published "
                    "in Ubuntu. No %s created.",
                    pkgname,
                    ref,
                )
            else:
                logging.debug(
                    "Setting %s to '%s'",
                    ref,
                    commit,
                )
                repo.update_head_to_commit(ref, commit)


def get_import_tag_msg():
    return 'git-ubuntu import'


def create_applied_tag(repo, commit_hash, version, namespace):
    tag_msg = get_import_tag_msg()

    existing_applied_tag = repo.get_import_tag(
        version,
        namespace,
        PatchState.APPLIED,
    )
    if existing_applied_tag:
        base_applied_reimport_tag = repo.get_reimport_tag(
            version,
            namespace,
            reimport=0,
            patch_state=PatchState.APPLIED,
        )
        if base_applied_reimport_tag:
            assert (
                base_applied_reimport_tag.peel(pygit2.Commit).id ==
                existing_applied_tag.peel(pygit2.Commit).id
            )
        else:
            repo.create_tag(
                str(existing_applied_tag.peel(pygit2.Commit).id),
                reimport_tag(
                    version,
                    namespace,
                    reimport=0,
                    patch_state=PatchState.APPLIED,
                ),
                tag_msg,
            )
        num_existing_applied_reimports = len(
            repo.get_all_reimport_tags(version, namespace, PatchState.APPLIED)
        )
        assert not repo.get_reimport_tag(
            version,
            namespace,
            reimport=num_existing_applied_reimports,
            patch_state=PatchState.APPLIED,
        )
        repo.create_tag(
            commit_hash,
            reimport_tag(
                version,
                namespace,
                reimport=num_existing_applied_reimports,
                patch_state=PatchState.APPLIED,
            ),
            tag_msg,
        )
        return repo.get_reimport_tag(
            version,
            namespace,
            reimport=num_existing_applied_reimports,
            patch_state=PatchState.APPLIED,
        )
    else:
        repo.create_tag(
            commit_hash,
            import_tag(version, namespace, PatchState.APPLIED),
            tag_msg,
        )
        return repo.get_import_tag(version, namespace, PatchState.APPLIED)


def create_import_tag(repo, commit, version, namespace):
    """Create an import tag in the repository

    If an import tag for this package version already exists, then a reimport
    tag is created instead, according to the specification.

    :param GitUbuntuRepository repo: the repository to create the tag in.
    :param pygit2.Commit commit: the commit hash the tag will point to.
    :param str version: the package version string of the import tag to create.
    :param str namespace: the namespace under which relevant refs can be found.
    :returns: None.
    """
    tag_msg = get_import_tag_msg()
    tagger = pygit2.Signature(
        spec.SYNTHESIZED_COMMITTER_NAME,
        spec.SYNTHESIZED_COMMITTER_EMAIL,
        commit.committer.time,
        commit.committer.offset,
    )

    existing_import_tag = repo.get_import_tag(
        version,
        namespace,
    )
    if existing_import_tag:
        base_reimport_tag = repo.get_reimport_tag(
            version,
            namespace,
            reimport=0,
        )
        if base_reimport_tag:
            assert (
                base_reimport_tag.peel(pygit2.Commit).id ==
                existing_import_tag.peel(pygit2.Commit).id
            )
        else:
            repo.create_tag(
                str(existing_import_tag.peel(pygit2.Commit).id),
                reimport_tag(version, namespace, reimport=0),
                tag_msg,
                tagger=existing_import_tag.peel(pygit2.Tag).tagger,
            )
        num_existing_reimports = len(
            repo.get_all_reimport_tags(version, namespace)
        )
        assert not repo.get_reimport_tag(
            version,
            namespace,
            reimport=num_existing_reimports,
        )
        repo.create_tag(
            str(commit.id),
            reimport_tag(version, namespace, reimport=num_existing_reimports),
            tag_msg,
            tagger=tagger,
        )
        return repo.get_reimport_tag(
            version,
            namespace,
            reimport=num_existing_reimports,
        )
    else:
        repo.create_tag(
            str(commit.id),
            import_tag(version, namespace),
            tag_msg,
            tagger=tagger,
        )
        return repo.get_import_tag(version, namespace)


def get_changelog_parent_commits(
    repo,
    namespace,
    parent_overrides,
    import_tree_versions,
    patch_state,
):
    """Given a changelog history, find the changelog parent commits

    "Changelog parents" are defined in the import specification, which refers
    to source package data instances. Since this importer runs incrementally in
    Launchpad publication order, the commits associated with the source package
    data instances can be located by traversing the matching reimport tags in
    order. If there are no reimport tags, the sole import tag (if it exists)
    will match the sole corresponding source package data instance. Otherwise
    we find no changelog parent and return an empty list.

    This function performs this search and returns a list of commit hashes that
    are the changelog parents.

    :param gitubuntu.git_repository.GitUbuntuRepository repo: the repository to
        use
    :param dict parent_overrides: see parse_parentfile
    :param list(str) import_tree_versions: list of package version strings as
        parsed from debian/changelog in the same order
    :param PatchState patch_state: whether to look for previous unapplied or
        applied imports

    :rtype: list(str)
    :returns: a list of commit hashes of any existing imports of the changelog
        parents as defined by the import specification
    """
    # skip current version
    import_tree_versions = import_tree_versions[1:]

    for version in import_tree_versions:
        changelog_parent_tags = get_existing_import_tags(
            repo,
            version,
            namespace,
            patch_state,
        )

        if not changelog_parent_tags:
            continue

        # coherence check that version from d/changelog of the
        # tagged parent matches ours
        changelog_parent_versions = [
            repo.get_changelog_versions_from_treeish(
                str(tag.peel(pygit2.Tree).id)
            )[0] for tag in changelog_parent_tags
        ]

        # if the parent was imported via a parent override
        # itself, assume it is valid without checking its
        # tree's debian/changelog, as it wasn't used
        if (version in parent_overrides or
            any(
                [changelog_parent_version == version
                 for changelog_parent_version in changelog_parent_versions]
            )
        ):
            changelog_parent_commits = [
                str(tag.peel(pygit2.Commit).id)
                for tag in changelog_parent_tags
            ]

            logging.debug("Changelog parent(s) (tag(s)) are %s",
                [
                    repo.tag_to_pretty_name(tag)
                    for tag in changelog_parent_tags
                ]
            )
            return changelog_parent_commits

    return []


def get_unapplied_import_parents(
    repo,
    version,
    namespace,
    parent_overrides,
    unapplied_import_tree_hash,
):
    """
    Determine the parents to be used for a synthesized unapplied import commit.

    :param gitubuntu.GitUbuntuRepository repo: the repository that is the
        target of this import.
    :param str version: the changelog version of the package being imported.
    :param str namespace: the namespace under which relevant refs can be found.
    :param dict parent_overrides: see parse_parentfile()
    :param str unapplied_import_tree_hash: the hash of tree object in the
        repository of the source tree being imported.
    :rtype: list(str)
    :returns: a list of commit hashes to use as the parents of the unapplied
        import commit.
    :raises ParentOverrideError: if there is a problem in finding a parent
        specified by a parent override.
    """
    if version in parent_overrides:
        logging.debug(
            '%s is specified in the parent override file.',
            version
        )

        return override_parents(
            parent_overrides,
            repo,
            version,
            namespace,
        )[0]
    else:
        import_tree_versions = repo.get_all_changelog_versions_from_treeish(
            unapplied_import_tree_hash
        )
        changelog_version = import_tree_versions[0]

        logging.debug(
            "Found changelog version %s in newly imported tree" %
            changelog_version
        )

        # coherence check on spph and d/changelog agreeing on the latest version
        if version not in parent_overrides:
            assert version == changelog_version, (
                'source pkg version: {} != changelog version: {}'.format(
                    version, changelog_version))

        return get_changelog_parent_commits(
            repo,
            namespace,
            parent_overrides,
            import_tree_versions,
            patch_state=PatchState.UNAPPLIED,
        )


class RichHistoryError(RuntimeError):
    """General superclass for rich history related errors"""
    pass


class RichHistoryTreeMismatch(RichHistoryError):
    """Rich history failed to validate because the tree provided doesn't match
    the Launchpad upload
    """
    pass


class RichHistoryHasNoChangelogParentAncestor(RichHistoryError):
    """Rich history failed to validate because no changelog parent is its
    ancestor
    """
    pass


class RichHistoryFetchFailure(RichHistoryError):
    """Rich history could not be downloaded

    This might be due to an Internet connectivity problem or a Launchpad outage
    or timeout, for example. Retries may be appropriate.
    """
    pass


class RichHistoryNotFoundError(RichHistoryError):
    """Rich history was specified but could not be found"""
    pass


class RichHistoryNotACommitError(RichHistoryError):
    """Rich history was specified but is an object that is not a commit"""
    pass


class RichHistoryHasUnacceptableSource(RichHistoryError):
    """Rich history is specified to be from an untrusted source"""
    pass


class RichHistoryInputValidationError(RichHistoryError):
    """Rich history specified failed input validation"""
    pass


def validate_rich_history(
    repo,
    rich_history_commit,
    import_tree,
    changelog_parents,
):
    """Validate rich history according to the spec.

    The spec requires that:

    1. The tree of the upload matches the tree of the supplied rich history
       commit exactly.

    2. If there is a changelog parent, then at least one changelog parent is an
       ancestor of the supplied rich history commit (see the spec for the
       definition of a changelog ancestor).

    :param gitubuntu.GitUbuntuRepository repo: the repository that is the
        target of this import.
    :param pygit2.Commit rich_history_commit: the rich history to be validated.
    :param pygit2.Tree import_tree: the upload published by Launchpad.
    :param list(str) changelog_parents: the changelog parent commit hash
        strings of the upload published by Launchpad.
    :raises RichHistoryTreeMismatch: if the rich history fails to validate
        because the tree provided doesn't match the Launchpad upload.
    :raises RichHistoryHasNoChangelogParentAncestor: if the rich history fails
        to validate because no changelog parent is its ancestor.
    :rtype: bool
    :returns: True if the rich history validates. If validation fails, then an
        exception is raised so the function never returns.
    """
    if rich_history_commit.tree.id != import_tree.id:
        raise RichHistoryTreeMismatch()

    if not changelog_parents:
        return True

    has_changelog_parent_ancestor = any(
        repo.raw_repo.descendant_of(
            rich_history_commit.id,
            changelog_parent_commit,
        )
        for changelog_parent_commit in changelog_parents
    )
    if not has_changelog_parent_ancestor:
        raise RichHistoryHasNoChangelogParentAncestor()

    return True


def add_changelog_note_to_commit(
    repo,
    namespace,
    commit,
    changelog_parents,
):
    """Compute and add a changelog note to the given commit.

    For user convenience, changelog notes allow for "git log" and similar tools
    to display the debian/changelog entry associated with a
    importer-synthesized commit. See LP #1633114 for details.

    If a changelog note already exists for the given commit, then return
    without doing anything.

    :param gitubuntu.GitUbuntuRepository repo: the repository that is the
        target of this import.
    :param str namespace: the namespace under which relevant refs can be found.
    :param pygit2.Commit commit: the commit for which the changelog note should
        be calculated and added.
    :param list(str) changelog_parents: the list of commit hashes that are the
        parents of the provided commit, from which the changelog note may be
        calculated.
    :returns: None
    """
    # If the note already exists, return without doing anything.
    try:
        repo.raw_repo.lookup_note(
            str(commit.id),
            'refs/notes/%s/changelog' % namespace,
        )
    except KeyError:
        pass
    else:
        return

    # The note doesn't exist, so figure out what it should be and add it.
    changelog_summary = get_changelog_for_commit(
        repo=repo,
        tree_hash=str(commit.peel(pygit2.Tree).id),
        changelog_parent_commits=changelog_parents,
    )
    changelog_signature = pygit2.Signature(
        spec.SYNTHESIZED_COMMITTER_NAME,
        spec.SYNTHESIZED_COMMITTER_EMAIL,
    )

    # pygit2 only accepts a str as the message of the note, but we have
    # historical changelog entries that used non-unicode encodings from prior
    # to Debian defining that the file must be UTF-8. Ideally we'd just pass
    # through the unknown encoding since git doesn't care, but pygit2 doesn't
    # support this. Instead, for now we can work around by replacing the
    # un-decodable characters. This does lose fidelity, but since it's in a git
    # note it does not impact on anything apart from the individual affected
    # note and such notes can be updated later if required. See
    # https://github.com/libgit2/pygit2/issues/1021 for the upstream pygit2
    # issue. The loss in fidelity here is tracked in LP: #1888254.
    #
    # Examples of affected changelog entries:
    #     autoconf2.13 2.13-55
    #     autogen 1:5.8.3-2
    #     dbconfig-common 1.8.17
    #     dpatch 2.0.10
    #     dput 0.9.2.16ubuntu1
    #     emacs-goodies-el 24.9-2
    #     evolution 2.10.1-0ubuntu2
    #     gmp 2:4.2.2+dfsg-3ubuntu1
    #     gnome-session 2.17.92-0ubuntu2
    #     gnupg 1.4.3-2ubuntu1
    #     iptables 1.3.5.0debian1-1ubuntu1
    #     jadetex 3.13-2.1ubuntu2
    #     llvm-toolchain-3.9 1:3.9.1-4ubuntu3~14.04.2
    changelog_summary_string = changelog_summary.decode(errors='replace')

    repo.raw_repo.create_note(
        changelog_summary_string,
        changelog_signature,
        changelog_signature,
        str(commit.id),
        'refs/notes/%s/changelog' % namespace,
    )


def create_import_note(
    repo,
    commit,
    namespace,
    timestamp=None,
):
    """Add an import note for the given commit.

    Import notes hold any metadata that we want to store at import time for
    future diagnostics, for example. For now, we store just the version of the
    importer that synthesized or adopted the corresponding commit together with
    a timestamp. This is done outside the commits or tags themselves so as not
    to affect hash stability.

    :param gitubuntu.GitUbuntuRepository repo: the repository that is the
        target of this import.
    :param pygit2.Commit commit: the newly synthesized commit for which the
        note should be stored.
    :param str namespace: the namespace under which relevant refs can be found.
    :param datetime.datetime timestamp: the timestamp of the import to specify
        in the note. If None, then the current time is used.
    :returns: None
    """
    if timestamp is None:
        timestamp = datetime.datetime.now()

    signature = pygit2.Signature(
        spec.SYNTHESIZED_COMMITTER_NAME,
        spec.SYNTHESIZED_COMMITTER_EMAIL,
    )
    repo.raw_repo.create_note(
        "Imported by git-ubuntu import %s at %s\n" %
            (VERSION, timestamp.isoformat()),
        signature,
        signature,
        str(commit.id),
        'refs/notes/%s/importer' % namespace,
    )


def parse_rich_vcs_url_from_changes(changes):
    """Extract and validate the rich history git URL to use

    This provides our protection against malicious URLs, such as those that use
    us to abuse other services, or that might subvert our behaviour.

    Notable pitfall protected against here: git will execute arbitrary code if
    the URL uses its ext:: scheme. See git-remote-ext(1).

    Example valid changes file entry:

        Vcs-Git: https://git.launchpad.net/~racb/ubuntu/+source/hello

    See tests for examples of other valid and invalid entries.

    :param debian.deb822.Changes changes: the parsed changes file specifying
        rich history.
    :rtype: str
    :returns: the git URL from which to fetch rich history
    :raises RichHistoryInputValidationError: if the URL in the Changes object
        fails to validate according to our requirements.
    """
    url = next(iter(word for word in changes['Vcs-Git'].split(' ') if word))
    if not VCS_GIT_URL_VALIDATION.fullmatch(url):
        raise RichHistoryInputValidationError()
    return url


def parse_rich_vcs_ref_from_changes(changes):
    """Extract and validate the rich history git ref to fetch

    This provides our protection against malicious refs, such as those that use
    us to abuse other services, or that might subvert our behaviour.

    Notable pitfalls protected against here: a ref is actually a refspec that
    starts with '+' or contains ':' characters would cause us to force fetch
    and/or overwrite local refs respectively.

    Example valid changes file entry:

        Vcs-Git-Ref: refs/heads/feature-branch-name

    See tests for examples of other valid and invalid entries.

    :param debian.deb822.Changes changes: the parsed changes file specifying
        rich history.
    :rtype: str
    :returns: the git refs in which to find rich history
    :raises RichHistoryInputValidationError: if the git refs in the Changes
        object fail to validate according to our requirements.
    """
    ref = next(iter(
        word for word in changes['Vcs-Git-Ref'].split(' ') if word
    ))
    if not VCS_GIT_REF_VALIDATION.fullmatch(ref):
        raise RichHistoryInputValidationError()
    try:
        # We aren't using git_repository.git_run() or
        # git_repository.GitUbuntuRepository.git_run() here because they are
        # tied to a particular repository to run against, and we don't have
        # that as a parameter to this function here. Nor do we need it, since
        # ref validation is independent of any specific repository or local
        # configuration. It's therefore not necessary to set the environment up
        # as the git_run functions do. Calling 'git' directly is sufficient.
        #
        # For now, we don't use --refspec-pattern since we don't accept
        # wildcards. We might enhance the spec to accept wildcards in the
        # future, in which case --refspec-pattern would need to be added to the
        # "git check-ref-format" here.
        #
        # shell=False is very important here to avoid malicious injection, so
        # we state it explicitly.
        run(['git', 'check-ref-format', ref], shell=False)
    except CalledProcessError as e:
        raise RichHistoryInputValidationError() from e
    return ref


def parse_rich_vcs_commit_from_changes(changes):
    """Extract and validate the rich history commit hash

    This provides our protection against malicious commit hashes.

    There are no specifically known exploits protected against here. However,
    given that a commit hash must be unabbreviated and is always a hex string,
    it makes sense to check it is exactly a hex string.

    Example valid changes file entry:

        Vcs-Git-Commit: 7a9255b27e0c78f6ae1d438f9b90e136b872e5f9

    See tests for examples of other valid and invalid entries.

    :param debian.deb822.Changes changes: the parsed changes file specifying
        rich history.
    :rtype: str
    :returns: the git commit that is the rich history to use
    :raises RichHistoryInputValidationError: if the commit hash in the Changes
        object fails to validate according to our requirements.
    """
    commit = next(iter(
        word for word in changes['Vcs-Git-Commit'].split(' ') if word
    ))
    if not VCS_GIT_COMMIT_VALIDATION.fullmatch(commit):
        raise RichHistoryInputValidationError()
    return commit


def fetch_rich_history_from_changes_file(
    repo,
    spi,
    import_tree,
    changelog_parents,
    retry_wait=tenacity.wait_fixed(3600),
    max_retries=3,
):
    """Import rich history if available from a changes file

    If the provided GitUbuntuSourcePackageInformation object has a related
    changes file, and that changes file contains headers that specify where
    rich history associated with the same upload can be found, then retrieve
    that commit into the provided git repository and return the retrieved
    and validated pygit2.Commit object.

    If rich history is not available, then return None.

    If rich history is available but it is not valid, then raise an appropriate
    exception.

    Changes file headers that specify rich history are expected as follows:

    Vcs-Git: specifies the path to the repository (same as in debian/control
        files).
    Vcs-Git-Commit: specifies the commit hash in the repository that
        corresponds to this upload.
    Vcs-Git-Ref: a ref in Vcs-Git from which the commit specified in
        Vcs-Git-Commit is reachable.

    :param gitubuntu.GitUbuntuRepository repo: the repository into which the
        rich history will be retrieved, and that contains the tree against
        which the rich history will be validated.
    :param gitubuntu.source_information.SourcePackageInformation spi: the
        publishing record this import corresponds to. This must be provided and
        may not be None.
    :param pygit2.Tree import_tree: the tree object in the repository of the
        source tree being imported. Rich history will be validated against this
        tree.
    :param list(str) changelog_parents: the changelog parent commit hash
        strings of the upload published by Launchpad.
    :raises RichHistoryHasUnacceptableSource: if rich history cannot be
        permitted to be fetched from the URL provided by the changes file
        because it comes from an untrusted source.
    :raises RichHistoryFetchFailure: if rich history could not be downloaded
        from the repository specified by the changes file.
    :raises RichHistoryNotFoundError: if the ref specified by Vcs-Git-Ref was
        not found, or the commit specified by Vcs-Git-Commit was not found
        after fetching the ref.
    :raises RichHistoryNotACommitError: if the object corresponding to the
        Vcs-Git-Commit header in the changes file is a valid object but not a
        commit object.
    :raises RichHistoryTreeMismatch: if the rich history fails to validate
        because the tree provided doesn't match the Launchpad upload.
    :raises RichHistoryHasNoChangelogParentAncestor: if the rich history fails
        to validate because no changelog parent is its ancestor.
    :rtype: pygit2.Commit or None
    :returns: the validated rich history commit, or None if no rich history is
        specified by the changes file.
    """
    url = spi.get_changes_file_url()
    if not url:
        # Not all Launchpad source_package_publishing_history objects provide a
        # changes file. For example, if a package was imported from Debian,
        # then a changes file isn't available. In this case we can treat it as
        # "no rich history found".
        return None
    with urllib.request.urlopen(url) as f:
        changes = debian.deb822.Changes(f)

    if not changes.keys() >= {'Vcs-Git', 'Vcs-Git-Commit', 'Vcs-Git-Ref'}:
        return None

    rich_url = parse_rich_vcs_url_from_changes(changes)
    rich_ref = parse_rich_vcs_ref_from_changes(changes)
    rich_commit_hash = parse_rich_vcs_commit_from_changes(changes)

    if not rich_url.startswith(LAUNCHPAD_GIT_HOSTING_URL_PREFIX):
        # For now, we only trust Launchpad's git hosting. See
        # "transfer.fsckObjects" in git-config(1). To expand safely to any git
        # repository, some fairly elaborate precautions need to be taken first.
        raise RichHistoryHasUnacceptableSource()

    # Unable to use pygit2 here due to
    # https://github.com/libgit2/pygit2/issues/1060
    #
    # The retry logic here means that if somebody injects a failing URL to
    # fetch rich history from, the importer will effectively be DoSed while it
    # waits for retries. However, since we're currently only accepting
    # Launchpad Git URLs, this shouldn't be a problem in practice. In the
    # longer term we can implement soft fail in our queueing mechanism for
    # automatic retries without holding up the rest of the queue.
    try:
        for attempt in tenacity.Retrying(
            wait=retry_wait,
            retry=tenacity.retry_if_exception_type(CalledProcessError),
            stop=tenacity.stop_after_attempt(max_retries)
        ):
            with attempt:
                try:
                    repo.git_run(
                        ['fetch', rich_url, rich_ref],
                        # Since we parse the output, we must make sure the
                        # output will contain untranslated strings. If this
                        # prompts for auth, then we can just treat it as a
                        # failure, and it's generally wrong for Vcs-Git to
                        # require auth anyway, so we should just suppress the
                        # prompt (LP: #1980982).
                        env={'LC_ALL': 'C.UTF-8', 'GIT_TERMINAL_PROMPT': '0'},
                    )
                except CalledProcessError as e:
                    if (
                        e.stderr and
                        e.stderr.startswith(GIT_REMOTE_REF_MISSING_PREFIX)
                    ):
                        raise RichHistoryNotFoundError() from e
                    elif (
                        e.stderr and
                        e.stderr.startswith(GIT_REMOTE_AUTH_FAILED_PREFIX)
                    ):
                        raise RichHistoryNotFoundError() from e
                    else:
                        raise
    except tenacity.RetryError as e:
        raise RichHistoryFetchFailure() from e

    try:
        commit = repo.raw_repo[rich_commit_hash]
    except KeyError as e:
        raise RichHistoryNotFoundError() from e
    if not isinstance(commit, pygit2.Commit):
        raise RichHistoryNotACommitError()
    validate_rich_history(
        repo=repo,
        rich_history_commit=commit,
        import_tree=import_tree,
        changelog_parents=changelog_parents,
    )
    return commit


def find_or_create_unapplied_commit(
    repo,
    version,
    namespace,
    import_tree,
    parent_overrides,
    commit_date,
    rich_history_tmpdir=None,
    changelog_date_overrides=frozenset(),
    spi=None,
):
    """Return the commit to be used for an unapplied import

    If an appropriate commit can already be found in the repository, return it.
    Otherwise, synthesize a new commit and return that.

    An appropriate commit is either the previous import of this package version
    (if its source tree matches exactly), or a valid rich history commit left
    by an uploader.

    If this import previously existed and its commit is being returned, then
    also return the corresponding import tag. Otherwise no tag is returned.
    Note that this is the import tag, and not related to upload tags. If a
    previous import didn't exist and an upload tag exists, this function
    nevertheless returns no tag.

    :param gitubuntu.GitUbuntuRepository repo: the repository that is the
        target of this import.
    :param str version: the changelog version of the package being imported.
    :param str namespace: the namespace under which relevant refs can be found.
    :param pygit2.Tree import_tree: the tree object in the repository of the
        source tree being imported.
    :param dict parent_overrides: see parse_parentfile()
    :param str commit_date: committer date to use for the git commit metadata.
        If None, use the current date.
    :param frozenset(str) changelog_date_overrides: versions from the changelog
        where the changelog's date should be ignored, and the commit date to be
        used instead. See the docstring of parse_changelog_date_overrides() for
        details.
    :param gitubuntu.source_information.SourcePackageInformation spi: the
        publishing record this import corresponds to. This may be None if one
        isn't known or doesn't exist.
    :rtype: tuple(pygit2.Commit, pygit2.Reference or None)
    :returns: the commit that has been found or created, and the import tag
        corresponding to an existing import if it had previously existed.
    """
    import_tags = get_existing_import_tags(repo, version, namespace)
    for candidate_tag in import_tags:
        if candidate_tag.peel().tree.id == import_tree.id:
            # An existing import tag matches our imported tree, so we can use
            # this tag and corresponding commit instead of creating a new
            # commit
            return candidate_tag.peel(), candidate_tag

    changelog_parents = get_unapplied_import_parents(
        repo=repo,
        version=version,
        namespace=namespace,
        parent_overrides=parent_overrides,
        unapplied_import_tree_hash=str(import_tree.id),
    )

    # If reimporting, attempt to import any upload tag that might be importable
    if rich_history_tmpdir:
        try:
            rich_history.import_single(
                repo=repo,
                path=rich_history_tmpdir,
                version=version,
                namespace=namespace,
            )
        except FileNotFoundError:
            pass
        except rich_history.BaseNotFoundError:
            pass

    # Try looking for a valid upload tag
    upload_tag = repo.get_upload_tag(version, namespace)
    if upload_tag:
        try:
            validate_rich_history(
                repo=repo,
                rich_history_commit=upload_tag.peel(),
                import_tree=import_tree,
                changelog_parents=changelog_parents,
            )
        except RichHistoryError as e:
            logging.warning(
                "Found upload tag %s, but rich history inclusion failed with "
                "%r. Will import %s as normal, ignoring the upload tag.",
                repo.tag_to_pretty_name(upload_tag),
                e,
                version,
            )
        else:
            return upload_tag.peel(), None

    # Try fetching rich history from a git repository if referenced in the
    # changes file
    if spi:
        try:
            rich_history_commit = fetch_rich_history_from_changes_file(
                repo=repo,
                spi=spi,
                import_tree=import_tree,
                changelog_parents=changelog_parents,
            )
        except RichHistoryFetchFailure:
            # This might be a soft fail (eg. an Internet or Launchpad timeout
            # or outage) so fail the import, allowing for future retry, rather
            # than continue without the rich history.
            raise
        except RichHistoryError as e:
            # Any other type of rich history failure is permanent, so continue
            # without the rich history.
            logging.warn(
                "Found rich history for %s, but rich history inclusion failed "
                "with %r. Will import %s as normal, ignoring the rich "
                "history.",
                spi,
                e,
                version,
            )
        else:
            if rich_history_commit:
                return rich_history_commit, None

    # Create a commit
    commit = _commit_import(
        repo=repo,
        version=version,
        tree_hash=str(import_tree.id),
        namespace=namespace,
        changelog_parent_commits=changelog_parents,
        # unapplied trees do not have a distinct unapplied parent
        unapplied_parent_commit=None,
        commit_date=commit_date,
        author_date=(
            # If being overridden, use commit_date for the authorship date.
            # Otherwise use None, which means that the author date should be
            # derived from the changelog of the tree being committed. See
            # parse_changelog_date_overrides() for details.
            commit_date if version in changelog_date_overrides else None
        ),
    )
    logging.debug(
        "Committed patches-unapplied import of %s as %s",
        version,
        str(commit.id),
    )
    add_changelog_note_to_commit(
        repo=repo,
        namespace=namespace,
        commit=commit,
        changelog_parents=changelog_parents,
    )
    return commit, None


def import_unapplied_dsc(
    repo,
    version,
    namespace,
    dist,
    dsc_pathname,
    head_name,
    skip_orig,
    parent_overrides,
    commit_date=None,
    rich_history_tmpdir=None,
    changelog_date_overrides=frozenset(),
    spi=None,
):
    """Imports a DSC into a Git repository without patches applied

    :param repo gitubuntu.GitUbuntuRepository The Git repository into
         which the source package contents corresponding to @spi should
         be imported.
    :param version str Changelog version
    :param namespace str Namespace under which relevant refs can be
         found.
    :param dist str Either 'debian' or 'ubuntu' or None
    :param dsc_pathname str Filesystem path to a DSC file
    :param head_name str Git branch name to import the DSC to
    :param skip_orig bool If True, do not attempt to import orig
         tarballs. Useful for debug import runs to make them as fast as
         possible, or to workaround pristine-tar issues.
    :param parent_overrides dict See parse_parentfile.
    :param str commit_date: committer date to use for the git commit metadata.
        If None, use the current date.
    :param frozenset(str) changelog_date_overrides: versions from the changelog
        where the changelog's date should be ignored, and the commit date to be
        used instead. See the docstring of parse_changelog_date_overrides() for
        details.
    :param gitubuntu.source_information.SourcePackageInformation spi: the
        publishing record this dsc corresponds to. This may be None if one
        isn't known or doesn't exist.
    :rtype None
    """
    import_dsc(
        repo,
        GitUbuntuDsc(dsc_pathname),
        namespace,
        version,
        dist,
    )
    if not skip_orig:
        try:
            import_orig(
                repo,
                GitUbuntuDsc(dsc_pathname),
                namespace,
                version,
                dist,
            )
        except GitUbuntuImportOrigError as e:
            logging.warning(
                "Unable to pristine-tar import orig tarball for %s",
                version,
            )

    unapplied_import_tree_hash = dsc_to_tree_hash(
        repo.raw_repo,
        dsc_pathname,
    )
    logging.debug(
        "Imported patches-unapplied version %s as tree %s",
        version,
        unapplied_import_tree_hash
    )

    try:
        commit, tag = find_or_create_unapplied_commit(
            repo=repo,
            version=version,
            namespace=namespace,
            import_tree=repo.raw_repo.get(unapplied_import_tree_hash),
            parent_overrides=parent_overrides,
            commit_date=commit_date,
            rich_history_tmpdir=rich_history_tmpdir,
            changelog_date_overrides=changelog_date_overrides,
            spi=spi,
        )
    except ParentOverrideError as e:
        logging.error("%s" % e)
        return

    if not tag:
        # find_or_create_unapplied_commit() did not return a tag, which means
        # that this source hadn't previously been imported. Either rich history
        # has been adopted, or a new commit has been synthesized. We must
        # therefore add an import tag pointing to this commit.
        create_import_tag(
            repo=repo,
            commit=commit,
            version=version,
            namespace=namespace,
        )
        # Add a diagnostic git note to correspond to this new import tag.
        create_import_note(
            repo=repo,
            commit=commit,
            namespace=namespace,
        )

    repo.update_head_to_commit(
        head_name,
        str(commit.id),
    )

# imports a package based upon source package information
def import_unapplied_spi(
    repo,
    spi,
    namespace,
    skip_orig,
    parent_overrides,
    rich_history_tmpdir=None,
    changelog_date_overrides=frozenset(),
):
    """Imports a publication record from Launchpad into the git
    repository

    :param repo gitubuntu.GitUbuntuRepository The Git repository into
         which the source package contents corresponding to @spi should
         be imported.
    :param spi gitubuntu.source_information.SourcePackageInformation
         The publishing record to import.
    :param namespace str Namespace under which relevant refs can be
         found.
    :param skip_orig bool If True, do not attempt to import orig
         tarballs. Useful for debug import runs to make them as fast as
         possible, or to workaround pristine-tar issues.
    :param parent_overrides dict See parse_parentfile.
    :param frozenset(str) changelog_date_overrides: versions from the changelog
        where the changelog's date should be ignored, and the commit date to be
        used instead. See the docstring of parse_changelog_date_overrides() for
        details.
    :rtype None
    """
    pretty_head_name = spi.pretty_head_name

    logging.info('Importing patches-unapplied %s to %s',
        spi.version, pretty_head_name
    )

    spi.pull()

    repo.clean_repository_state()

    import_unapplied_dsc(
        repo=repo,
        version=str(spi.version),
        namespace=namespace,
        dist=spi.distribution_name.lower(),
        dsc_pathname=spi.dsc_pathname,
        head_name=spi.head_name(namespace),
        skip_orig=skip_orig,
        parent_overrides=parent_overrides,
        commit_date=spi.date_created,
        rich_history_tmpdir=rich_history_tmpdir,
        changelog_date_overrides=changelog_date_overrides,
        spi=spi,
    )

def import_applied_dsc(
    repo,
    version,
    namespace,
    dist,
    dsc_pathname,
    head_name,
    allow_applied_failures,
    parent_overrides,
    commit_date=None,
):
    """Imports a DSC into a Git repository with patches applied

    :param repo gitubuntu.GitUbuntuRepository The Git repository into
         which the source package contents corresponding to @spi should
         be imported.
    :param version str Changelog version
    :param namespace str Namespace under which relevant refs can be
         found.
    :param dist str Either 'debian' or 'ubuntu' or None
    :param dsc_pathname str Filesystem path to a DSC file
    :param head_name str Git branch name to import the DSC to
    :param allow_applied_failures bool If True, failure to apply quilt
         patches is not fatal.
    :param parent_overrides dict See parse_parentfile.
    :param str commit_date: committer date to use for the git commit metadata.
        If None, use the current date.

    :rtype None
    """
    applied_tip_head = repo.get_head_by_name(head_name)
    _, _, pretty_head_name = head_name.partition('%s/' % namespace)

    unapplied_parent_tag = repo.get_import_tag(version, namespace)
    unapplied_parent_commit = unapplied_parent_tag.peel().id
    unapplied_import_tree_hash = str(unapplied_parent_tag.peel(pygit2.Tree).id)

    # this is the result of all patches being applied
    applied_import_tree_hash = dsc_to_tree_hash(
        repo.raw_repo,
        dsc_pathname,
        patches_applied=True,
    )

    logging.debug(
        "Found patches-unapplied version %s as commit %s",
        version,
        str(unapplied_parent_commit),
    )

    import_tree_versions = repo.get_all_changelog_versions_from_treeish(
                               unapplied_import_tree_hash
                                                                       )
    changelog_version = import_tree_versions[0]

    applied_tip_version = None
    # check if the version to import has already been imported to
    # this head
    if applied_tip_head is not None:
        if repo.treeishs_identical(
                unapplied_import_tree_hash, str(applied_tip_head.peel().id)
            ):
            logging.warning('%s is identical to %s', pretty_head_name, version)
            return
        applied_tip_version, _ = repo.get_changelog_versions_from_treeish(
                             str(applied_tip_head.peel().id),
                                                                 )

    logging.debug('Tip version is %s', applied_tip_version)

    if version in parent_overrides:
        logging.debug('%s is specified in the parent override file.' %
                     version
                    )

        (
            _,
            applied_changelog_parent_commits,
        ) = override_parents(
            parent_overrides,
            repo,
            version,
            namespace,
         )
    else:
        if version_compare(version, applied_tip_version) <= 0:
            logging.warning(
                'Version to import (%s) is not after %s tip (%s)',
                 version,
                 pretty_head_name,
                 applied_tip_version,
            )


        applied_changelog_parent_commits = get_changelog_parent_commits(
            repo,
            namespace,
            parent_overrides,
            import_tree_versions,
            patch_state=PatchState.APPLIED,
        )

    existing_applied_tag = repo.get_import_tag(
        version,
        namespace,
        PatchState.APPLIED,
    )
    if existing_applied_tag:
        # XXX: What should be checked here? The result of pushing all
        # the patches? How to do it most non-destructively? (Might be
        # able to use our new helper to get treeish after running a
        # command, in this case, `quilt push -a`)
        # if str(applied_tag.peel().tree.id) != applied_import_tree_hash:
        #     logging.error(
        #         "Found patches-applied import tag %s, but the "
        #         "corresponding tree does not match the published "
        #         "version. Will orphan commit.",
        #        repo.tag_to_pretty_name(applied_tag),
        #    )
        #    applied_changelog_parent_commits = []
        #else:
        repo.update_head_to_commit(
            head_name,
            str(existing_applied_tag.peel().id),
        )
        return

    # Assume no patches to apply
    applied_import_tree_hash = unapplied_import_tree_hash
    # get tree id from above commit
    try:
        for (
           applied_import_tree_hash,
           patch_name,
           patch_desc
        ) in import_patches_applied_tree(repo, dsc_pathname):
            # special case for .pc removal
            if patch_name is None:
                msg = b'%b.' % (patch_desc.encode())
            else:
                if patch_desc is None:
                    patch_desc = (
                        "%s\n\nNo DEP3 Subject or Description header found" %
                         patch_name
                    )
                msg = b'%b\n\nGbp-Pq: %b.' % (
                    patch_desc.encode(),
                    patch_name.encode()
                )

            unapplied_parent_commit = repo.commit_source_tree(
                tree=pygit2.Oid(hex=applied_import_tree_hash),
                parents=[unapplied_parent_commit],
                log_message=msg,
                commit_date=commit_date,
            )

            logging.debug(
                "Committed patch-application of %s as %s",
                patch_name, str(unapplied_parent_commit)
            )
    except CalledProcessError as e:
        if allow_applied_failures:
            return
        raise

    commit_hash = str(_commit_import(
        repo=repo,
        version=version,
        tree_hash=applied_import_tree_hash,
        namespace=namespace,
        changelog_parent_commits=applied_changelog_parent_commits,
        unapplied_parent_commit=str(unapplied_parent_commit),
        commit_date=commit_date,
    ).id)
    logging.debug(
        "Committed patches-applied import of %s as %s in %s",
        version,
        commit_hash,
        pretty_head_name,
    )

    tag = None
    if repo.get_import_tag(version, namespace, PatchState.APPLIED) is None:
        # Not imported before
        tag = import_tag(version, namespace, PatchState.APPLIED)
    elif (
        not applied_changelog_parent_commits and
        unapplied_parent_commit is None and
        head_name in repo.local_branch_names
    ):
        # No parents and not creating a new branch
        tag = orphan_tag(version, namespace)

    if tag:
        logging.debug("Creating tag %s pointing to %s", tag, commit_hash)
        repo.create_tag(
            commit_hash=commit_hash,
            tag_name=tag,
            tag_msg=get_import_tag_msg(),
        )

    repo.update_head_to_commit(
        head_name,
        commit_hash,
    )

def import_applied_spi(
    repo,
    spi,
    namespace,
    allow_applied_failures,
    parent_overrides,
):
    """Imports a DSC into a Git repository with patches applied

    :param repo gitubuntu.GitUbuntuRepository The Git repository into
         which the source package contents corresponding to @spi should
         be imported.
    :param spi gitubuntu.source_information.SourcePackageInformation
         The publishing record to import.
    :param namespace str Namespace under which relevant refs can be
         found.
    :param allow_applied_failures bool If True, failure to apply quilt
         patches is not fatal.
    :param parent_overrides dict See parse_parentfile.

    :rtype None
    """
    pretty_head_name = spi.pretty_head_name

    logging.info('Importing patches-applied %s to %s' % (spi.version, pretty_head_name))

    spi.pull()

    repo.clean_repository_state()

    import_applied_dsc(
        repo=repo,
        version=str(spi.version),
        namespace=namespace,
        dist=spi.distribution_name.lower(),
        dsc_pathname=spi.dsc_pathname,
        head_name=spi.applied_head_name(namespace),
        allow_applied_failures=allow_applied_failures,
        parent_overrides=parent_overrides,
        commit_date=spi.date_created,
    )


def import_publishes(
    repo,
    pkgname,
    namespace,
    patches_applied,
    debian_head_info,
    ubuntu_head_info,
    debian_sinfo,
    ubuntu_sinfo,
    active_series_only,
    workdir,
    skip_orig,
    allow_applied_failures,
    parent_overrides,
    rich_history_tmpdir=None,
    changelog_date_overrides=frozenset(),
):
    """
    :param frozenset(str) changelog_date_overrides: versions from the changelog
        where the changelog's date should be ignored, and the commit date to be
        used instead. See the docstring of parse_changelog_date_overrides() for
        details.
    """
    history_found = False
    srcpkg_information = None
    if patches_applied:
        _namespace = namespace
        namespace = '%s/applied' % namespace
        import_type = 'patches-applied'
        import_func =  functools.partial(
            import_applied_spi,
            allow_applied_failures=allow_applied_failures,
        )
    else:
        _namespace = namespace
        import_type = 'patches-unapplied'
        import_func =  functools.partial(
            import_unapplied_spi,
            skip_orig=skip_orig,
            rich_history_tmpdir=rich_history_tmpdir,
            changelog_date_overrides=changelog_date_overrides,
        )
    # Always look in Ubuntu for new publications
    gusi_head_info_tuple_list = [
        (ubuntu_sinfo, ubuntu_head_info),
    ]
    # Unless --active-series-only was specified, also look in Debian for new
    # publications.
    if not active_series_only:
        # For hash stability, we must process new Debian publications with the
        # same date_created before new Ubuntu publications, so we insert the
        # Debian entry before the Ubuntu entry.
        gusi_head_info_tuple_list.insert(
            0,
            (debian_sinfo, debian_head_info),
        )
    try:
        # We must process new publications in order of date_created interleaved
        # across both distributions (Debian first) in order to maintain hash
        # stability according to the spec.
        for srcpkg_information in GitUbuntuSourceInformation.interleave_launchpad_versions_published_after(
            gusi_head_info_tuple_list,
            namespace=namespace,
            workdir=workdir,
            active_series_only=active_series_only,
        ):
            history_found = True
            import_func(
                repo=repo,
                spi=srcpkg_information,
                namespace=_namespace,
                parent_overrides=parent_overrides,
            )
    except Exception as e:
        if srcpkg_information is None:
            msg = 'Unable to import %s' % import_type
        else:
            msg = 'Unable to import %s %s' % (import_type,
                 str(srcpkg_information.version))
        if not patches_applied:
            raise GitUbuntuImportError(msg) from e
        else:
            logging.error(msg)
            logging.error(traceback.format_exc())
    else:
        history_found = True

    return history_found


def parse_args(subparsers=None, base_subparsers=None):
    kwargs = dict(description='Update a launchpad git tree based upon '
                              'the state of the Ubuntu and Debian archives',
                  formatter_class=argparse.ArgumentDefaultsHelpFormatter
                 )
    if base_subparsers:
        kwargs['parents'] = base_subparsers
    if subparsers:
        parser = subparsers.add_parser('import', **kwargs)
        parser.set_defaults(func=cli_main)
    else:
        parser = argparse.ArgumentParser(**kwargs)
    parser.add_argument('package', type=str,
                        help='Which package to update in the git tree')
    parser.add_argument('-o', '--lp-owner', type=str,
                        help=argparse.SUPPRESS,
                        default='git-ubuntu-import')
    parser.add_argument('-l', '--lp-user', type=str,
                        help=argparse.SUPPRESS)
    parser.add_argument('--dl-cache', type=str,
                        help=('Cache directory for downloads. Default is to '
                              'put downloads in <directory>/.git/%s' %
                              CACHE_PATH
                             ),
                        default=argparse.SUPPRESS)
    parser.add_argument('--no-fetch', action='store_true',
                        help='Do not fetch from the remote (DANGEROUS; '
                             'only useful for debugging).')
    parser.add_argument('--push', action='store_true',
                        help='Push to the remote')
    parser.add_argument(
        '--set-as-default-repository',
        action='store_true',
        help='Set as default repository for this package in Launchpad after '
            'pushing. Requires --push.',
    )
    parser.add_argument('--no-clean', action='store_true',
                        help='Do not clean the temporary directory')
    parser.add_argument('-d', '--directory', type=str,
                        help='Use git repository at specified location rather '
                             'than a temporary directory (implies --no-clean). '
                             'Will create the local repository if needed.',
                        default=argparse.SUPPRESS
                       )
    parser.add_argument('--active-series-only', action='store_true',
                        help='Do an import of only active Ubuntu series '
                             'history.')
    parser.add_argument('--skip-applied', action='store_true',
                        help=argparse.SUPPRESS)
    parser.add_argument('--skip-orig', action='store_true',
                        help=argparse.SUPPRESS)
    parser.add_argument('--reimport', action='store_true',
                        help=argparse.SUPPRESS)
    parser.add_argument(
        '--allow-applied-failures',
        action='store_true',
        help=argparse.SUPPRESS,
    )
    if not subparsers:
        return parser.parse_args()
    return 'import - %s' % kwargs['description']

def cli_main(args):
    no_clean = args.no_clean
    try:
        directory = args.directory
        no_clean = True
    except AttributeError:
        directory = None
    try:
        user = args.lp_user
    except AttributeError:
        user = None
    try:
        dl_cache = args.dl_cache
    except AttributeError:
        dl_cache = None

    return main(
        pkgname=args.package,
        owner=args.lp_owner,
        no_clean=no_clean,
        no_fetch=args.no_fetch,
        push=args.push,
        active_series_only=args.active_series_only,
        skip_orig=args.skip_orig,
        skip_applied=args.skip_applied,
        reimport=args.reimport,
        allow_applied_failures=args.allow_applied_failures,
        directory=directory,
        user=user,
        proto=args.proto,
        dl_cache=dl_cache,
        pullfile=args.pullfile,
        parentfile=args.parentfile,
        retries=args.retries,
        retry_backoffs=args.retry_backoffs,
        changelog_date_override_file=args.changelog_date_override_file,
        set_as_default_repository=args.set_as_default_repository,
    )
