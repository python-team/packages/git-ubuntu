# Package versions that have illegal dates in their changelog entries.
# In these cases the first seen publication date must be used instead
# for the author date of a synthesized commit.
#
# Note: this file must exactly match the import specification. Before
# adding an entry here, adjust the specification first.

connect-proxy 1.100-1
dhcpcd 1:2.0.0-2
ghostscript 9.50~dfsg-5ubuntu4
gmsh 2.0.7-1.2ubuntu1
gnome-chemistry-utils 0.10.9-1ubuntu1
gsasl 0.0.11-1
ifupdown2 1.2.5-1
iscsitarget 0.4.15+svn148-2.1ubuntu1
jaraco.itertools 2.0.1-1
kaffeine 0.8.4-0ubuntu1
keychain 2.0.3-2
kopete-plugin-thinklight 0.3-0ubuntu1
lxqt-config 0.13.0-0ubuntu4
mail-spf-perl 2.004-0ubuntu1
nut 2.2.0-2
prips 0.9.4-3
prometheus-alertmanager 0.15.3+ds-3ubuntu1
scatterplot3d 0.3-33-2
software-properties 0.80
typedload 2.12-1
