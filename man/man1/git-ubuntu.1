.TH "GIT-UBUNTU" "1" "2017-07-19" "Git-Ubuntu 0.2" "Git-Ubuntu Manual"

.SH "NAME"
git-ubuntu \- use Git with a Launchpad hosted source package repository
.SH "SYNOPSIS"
.SP
.NF
\fIgit ubuntu\fR [\-h] [\-v | \-\-verbose] [\-\-retries <num_retries>]
[\-\-retry-backoffs <backoff,\&.\&.\&.>] [\-\-proto <proto>]
[\-P | \-\-parentfile <parentfile>] [\-L | \-\-pullfile <pullfile>]
<command> [<args>]
.FI
.SP
.SH "DESCRIPTION"
.SP
Ubuntu packages often have changes relative to their Debian bases,
referred to as the "Ubuntu delta"\&.
\fIgit ubuntu\fR represents a collection of tools for dealing with these
deltas, implemented as \fIgit\fR(1) subcommands\&.
.PP
Part of this work is performed automatically by the "importer", which
relies on the \fILaunchpad\fR repository as its source of truth about a given
source package's publishing history\&.
The remainder of the work needs to be handled manually, and
\fIgit ubuntu\fR provides a set of commands (detailed below) for doing
so\&.
.PP
A key concept to understand for the git ubuntu workflow is the
formalized use of git tags to track version states.  These tags will
look something like "logical/1%2.3.4.1-1ubuntu3"\&.  (Git and debian/changelog
conflict on the use of certain reserved characters, such as "~", ":"
and "."\&.  These are resolved following the Debian DEP14 version number
transformation standard\&.)
.PP
The tag types used by git ubuntu are "logical/", "split/",
'reconstruct/", "upload/", and 'format <format>"\&.
The first four forms are standardized for git-ubuntu usage and generate
tags, respectively, "logical/%(version)s", "split/%(version)s",
"reconstruct/%(version)s", "upload/%(versions)s", where "%(versions)"
will be replaced by the DEP14 mangling of the last version in
debian/changelog as present in <commitish>\&.
The last form allows free-form formatting\&.
.PP
The benefit of using these tools and this formalized tagging system is
to simplify future merges from Debian, allowing them to be done as
straightforward \fIgit rebase\fR operations\&.

.SP
.SH "OPTIONS"
.PP
The following options are common to all \fBgit-ubuntu\fR subcommands\&.
.PP
\-h, \-\-help
.RS 4
Prints the synopsis and a list of the most commonly used commands\&.
.RE
.PP
\-v, \-\-verbose
.RS 4
Makes \fIgit ubuntu\fR verbose\&.
Useful for debugging and seeing what underlying commands are run\&.
.RE
.PP
\-\-retries <num_retries>
.RS 4
Number of retries to attempt for network interactions with Launchpad\&.
The default is 5 retry attempts\&.
.RE
.PP
\-\-retry-backoffs <backoff,\&.\&.\&.>
.RS 4
Comma-separated list of backoff durations (in seconds) to use between
retry attempts\&.
The default is exponential backoff (with the default --retries value,
this corresponds to 1, 2 and 4 seconds between subsequent retry
attempts)\&.
.RE
.PP
\-\-proto <proto>
.RS 4
Protocol to use when fetching the configured repositories\&.
"git", "http" and "https" are valid options, with "https" the default\&.
.RE
.PP
\-p <parentfile>, \-\-parentfile <parentfile>
.RS 4
Path to a parent overrides file\&.
The import algorithm cannot always correctly determine the parenting
relationship between source package publications, particularly for
older historical publications\&.
\fIgit ubuntu\fR needs a mechanism to be told what the parents of a
publication are when they cannot be correctly deduced\&. The parentfile
specifies this information in the following format:
.RS 4
<pkgname> <child version> <publish parent version> <changelog parent version>
.RE
with one override per line\&. 
.SP
A default override file is provided with \fIgit ubuntu\fR and
end-users generally do not need to provide this option\&.
.RE
.PP
\-l <pullfile>, \-\-pullfile <pullfile>
.RS 4
Path to a pull overrides file\&.
If the Launchpad source package data is inaccurate
for a publish (this happens, for example, when a same-versioned source
package is published multiple times with different orig tarballs),
\fIgit ubuntu\fR needs a mechanism to be told where to obtain accurate
sources from\&.
The pullfile specifies this information in the following format:
.RS 4
<pkgname> <version> <url of dsc file> <urls of orig files>
.RE
with one package per line\&.
"-" for the dsc file indicates to use the one from launchpad\&.
.SP
A default override file is provided with \fIgit ubuntu\fR and
end-users generally do not need to provide this option\&.
.RE
.SP
.SH "GIT UBUNTU COMMANDS"
.PP
\fBgit-ubuntu-clone\fR(1)
.RS 4
Clone a repository into a new directory\&.
.RE
.PP
\fBgit-ubuntu-export-orig\fR(1)
.RS 4
Export orig tarballs from the Git repository\&.
.RE
.PP
\fBgit-ubuntu-import\fR(1)
.RS 4
Import the Launchpad publishing history of a source package into a
repository\&.
.RE
.PP
\fBgit-ubuntu-merge\fR(1)
.RS 4
Perform an Ubuntu merge\&.
.RE
.PP
\fBgit-ubuntu-queue\fR(1)
.RS 4
Interact with the unapproved queue\&.
.RE
.PP
\fBgit-ubuntu-remote\fR(1)
.RS 4
Manage tracked repositories\&.
.RE
.PP
\fBgit-ubuntu-submit\fR(1)
.RS 4
Submit changes to a repository as a Launchpad Merge Proposal\&.
.RE
.PP
\fBgit-ubuntu-tag\fR(1)
.RS 4
Create a tag object, respecting DEP14\&.
.RE
.SP
.SH "EXIT STATUS"
.SP
\fIgit ubuntu\fR exits with status 0 if a command completes
successfully\&.
\fIgit ubuntu\fR exits with nonzero exit status on errors\&.
.SP
.SH "FILES"
.PP
$GIT_DIR/\&.gitconfig, $HOME/\&.gitconfig
.RS 4
Some \fIgit ubuntu\fR configuration is stored via \fIgit config\fR in
both the global user and repository local level files\&.
Specificially:
.SP
.RS 4
gitubuntu\&.lpuser
.RE
Stores the Launchpad user name for authentication in the global user
file\&.
.RE
.SP
.SH "REPORTING BUGS"
.SP
Report bugs at
.UR https://bugs.launchpad.net/git-ubuntu
.UE
\&.
.PP
.SH "AUTHORS"
.SP
\fIgit ubuntu\fR was started conceptually by Robie Basak, and is
currently maintained by Nishanth Aravamudan\&.
Numerous contributions have been made by Scott Moser and other members
of the Ubuntu Server team\&.
.SP
.SH "SEE ALSO"
.SP
\fBgit\fR(1)
.PP
.UR https://wiki.ubuntu.com/UbuntuDevelopment/Merging/GitWorkflow
.UE
.PP
.\" Would prefer to use the following URL, but it is static
.\" .UR https://insights.ubuntu.com/2017/07/24/developing-ubuntu-using-git/
.\" .UE
.UR http://www.justgohome.co.uk/blog/2017/07/developing-ubuntu-using-git.html
.UE
.PP
.SH "GIT-UBUNTU"
Part of the \fBgit-ubuntu\fR(1) suite
