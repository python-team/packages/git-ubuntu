.TH "GIT-UBUNTU-IMPORT" "1" "2017-07-19" "Git-Ubuntu 0.2" "Git-Ubuntu Manual"

.SH "NAME"
git-ubuntu import \- Import Launchpad publishing history to Git
.SH "SYNOPSIS"
.SP
.NF
\fIgit ubuntu import\fR [\-o | --lp-owner <owner>] [\-l | \-\-lp-user
<user>] [\-\-dl-cache <dl_cache>] [\-\-no-fetch] [\-\-push]
[\-\-no-clean] [\-d | \-\-directory <directory>]
[\-\-active-series-only] [\-\-skip-applied] [\-\-skip-orig]
[\-\-reimport] [\-\-allow-applied-failures] <package>
.FI
.SP
.SH "DESCRIPTION"
.SP
Import a source package's publication history from Launchpad into a
local \fBgit\fR(1) repository and push it to a remote repository\&.
.PP
The importer algorithm roughly looks like:
.IP
.RS 4
Fetch the \fBowner\fR repository for \fB<package>\fR\&.
.PP
For <distribution> in "Debian", "Ubuntu"\&:
.RS 4
.PP
For each <publish> in <distribution> since the most recent
patches-unapplied import in the local repository\&:
.RS 4
.PP
Import the orig tarballs for <publish> using "gbp import-orig
--pristine-tar", unless they have already been imported and the imported
tarballs are identical.
.PP
Find a <changelog parent>: the most recent changelog entry with a
corresponding patches-unapplied import\&.
.PP
Find an <upload parent>: an upload-tag in the repository for the same
version as <publish> with git-tree-identical contents as <publish>\&.
.PP
Commit the git tree corresponding to a patches-unapplied extraction of
<publish> using <changelog parent> and <upload parent> as parents\&.
.PP
If this is the first patches-unapplied import of a given version, create
a tag in refs/tags/import/ pointing to the new commit\&.
.PP
Update the patches-applied <distribution>/<publish's series>/<publish's
pocket> branch to the new commit\&.
.RE
.RE
.RE
.PP
.RS 4
For <distribution> in "Debian", "Ubuntu"\&:
.PP
For each <publish> in <distribution> since the most recent
patches-applied import in the local repository\&:
.RS 4
.PP
Find a <changelog parent>: the most recent changelog entry with a
corresponding patches-applied import\&.
.PP
Find an <unapplied parent>: the corresponding patches-unapplied import of
<publish>'s version\&.
.PP
Commit each git tree corresponding to the application of the next quilt
patch in <publish> using <changelog parent> and <unapplied parent> as
parents\&.
As each quilt patch is applied, the <unapplied parent> becomes the
result of the immediately prior patch's application\&.
.PP
If this is the first patches-applied import of a given version, create
a tag in refs/tags/applied/ pointing to the commit corresponding to the
application of the final quilt patch\&.
.PP
Update the patches-applied <distribution>/<publish>'s series/<publish>'s
pocket branch to the new commit\&.
.RE
.RE
.RE
.SP
.SH "OPTIONS"
.PP
\-o <owner>, --lp-owner <owner>
.RS 4
The Launchpad owner of the remote respository to fetch the current
repository from and push the resulting import to.
.RE
.PP
\-l <user>, \-\-lp-user <user>
.RS 4
The Launchpad user to authenticate as for push URLs as well as
the default personal remote to create\&.
If not specified, this value is obtained from "git config --global
gitubuntu.lpuser"\&.
If that config value is not set, a prompt is shown to obtain the
value\&.
.RE
.PP
\-\-dl-cache <dl_cache>
.RS 4
The path to use for caching downloads from Launchpad\&.
By default, downloads are cached inside the repository's GIT_DIR\&.
.RE
.PP
\-\-no-fetch
.RS 4
Do not fetch the \fB<owner>\fR's repository before importing\&.
.RE
.PP
\-\-push
.RS 4
Confirm that you want to push the resulting import to the <\fB<owner>\fR's
repository after importing\&. This is not the default since for local purposes
operating outside the importer service this is not usually wanted\&.
.RE
.PP
\-\-no-clean
.RS 4
Do not delete local directory used for the import upon completion.
.RE
.PP
\-d <directory>, \-\-directory <directory>
.RS 4
The local directory to perform the import in\&.
Implies --no-clean\&.
If not specified, use a temporary directory\&.
.RE
.PP
\-\-active-series-only
.RS 4
Rather than importing all publications from Launchpad, only consider
those for the currently active series\&.
This can be useful to just get a quick view of the current state in the
archive, as a full import can take a very long time\&.
.RE
.PP
\-\-skip-applied
.RS 4
Only perform the patches-unapplied portion of the algorithm in
\fBDESCRIPTION\fR\&.
This can be useful to just get a quick view of the current state in the
archive, as a full import can take a very long time\&.
.RE
.PP
\-\-skip-orig
.RS 4
Do not attempt to import the orig tarballs\&.
This can be useful to just get a quick view of the current state in the
archive, as a full import can take a very long time\&.
This option can be useful for \fBgit-ubuntu\fR(1) developers when
debugging issues with "gbp" or "pristine-tar"\&.
.RE
.PP
\-\-reimport
.RS 4
Reimport the given source package from scratch\&.
The reimport will still incorporate upload tags as currently present in
the remote repository\&.
This option is only intended for \fBgit-ubuntu\fR(1) developers and is
used when the SHA1s of commits must change due to an implementation
change in \fIgit ubuntu-import\fR\&.
A typical reimport of a repository (for safety) will look like:
.PP
.RS 4
git ubuntu import --reimport --no-clean <package>
.PP
This will leave a local directory with the resulting reimport\&.
The local directory should be examined for the reimport's success; in
particular, that upload tags are reincorporated in the new history\&.
.PP
git ubuntu import --reimport --no-fetch --directory <tempdir from prior
command>
.PP
This will use the local directory, which is current as of the prior
reimport, ensure it is still current (in case new publishes have
occurred) and forcibly \fBgit-push\fR the resulting repository to
\fB<owner>\fR's remote repository.
.RE
.RE
.PP
\-\-allow-applied-failures
.RS 4
Do not treat a patch application as a hard failure when importing the
patches-applied history\&.
As \fBdpkg-source\fR(1)'s (or other tool's) behavior has evolved over
time, it is possible that the version used by \fBgit ubuntu import\fR
is not able to reproduce the patch application of a historical
publication\&.
By default, such a failure will stop the historical import for the
patches-applied branch, to allow a \fBgit ubuntu\fR developer to
investigate\&.
After investigation, this flag can be used to indicate the importer is
allowed to ignore such a failure\&.
.RE
.PP
<package>
.RS 4
The name of the source package to import\&.
.RE
.SP
.SH "EXIT STATUS"
.SP
\fIgit ubuntu import\fR exits with status 0 if the import completes
successfully\&.
\fIgit ubuntu import\fR exits with nonzero exit status on errors\&.
.SP
.SH "REPORTING BUGS"
.SP
Report bugs at
.UR https://bugs.launchpad.net/git-ubuntu
.UE
\.
.SP
.SH "SEE ALSO"
.SP
\fBquilt\fR(1)
.SP
.SH "GIT-UBUNTU"
Part of the \fBgit-ubuntu\fR(1) suite
