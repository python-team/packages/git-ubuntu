Explanation
===========

.. toctree::

    what_it_is
    design_goals_and_principles
    round_tripping
    testing
    keyring
