Keyring integration
===================

Some git-ubuntu subcommands (eg. ``git-ubuntu submit``) require authenticated
access to the Launchpad API. The first time you do this, you will be prompted
to authenticate in a web browser. If successful, the authenticated API key will
then be saved in your local keyring. You may be prompted for a password. This
password is used to encrypt your local keyring. If you don't yet have a local
keyring, the password you provide will be used to encrypt it. If you already
have a local keyring, the password you provide is the one required to decrypt
it.

See :doc:`/howto/keyring` for instructions on configuring keyring to use
plaintext password storage instead, to avoid getting keyring password prompts.

Implementation details
----------------------

git-ubuntu simply uses `launchpadlib
<https://help.launchpad.net/API/launchpadlib>`_ for Launchpad API access. This
library in turn uses the `Python keyring package
<https://pypi.org/project/keyring/>`_ for credential storage which is quite
configurable. If you see a password prompt, it is because the keyring package's
defaults in your particular environment require encrypted password-based
credential storage. You can configure this your needs by following the `keyring
documentation <https://pypi.org/project/keyring/>`_.
