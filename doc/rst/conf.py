project = 'git-ubuntu'
copyright = '2023 Canonical Ltd'
author = 'Robie Basak'
exclude_patterns = ['_build']
html_theme = 'sphinx_rtd_theme'
extensions = ['sphinx_design']
