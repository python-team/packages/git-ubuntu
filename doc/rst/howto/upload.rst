Upload to the Ubuntu archive with rich history
==============================================

Standard workflow
-----------------

These instructions make the following assumptions:

1. You have the git-ubuntu **beta** snap installed.
2. You had originally used :code:`git ubuntu clone` to clone your repository.
3. You have a local branch checked out and your working directory is clean.
4. A previous git-ubuntu commit is an ancestor of your current commit.
5. You are at the top level of your source tree that is ready to build.

Push your branch, create the source package and changes file and upload them as
follows::

    dpkg-buildpackage $(git ubuntu prepare-upload args) -S <your usual options>
    cd ..
    dput ubuntu <package>_<version>_source.changes

:code:`git ubuntu prepare-upload args` will push your branch to your namespace
on Launchpad with the same remote branch name, and then output the appropriate
arguments for :code:`dpkg-buildpackage` to include the appropriate reference to
the rich history in the changes file. See
:doc:`/reference/changes_file_headers`.

If you don't wish to retain your rich history branch in Launchpad, you must
wait for the importer to retrieve it before you delete it. To be sure of the
timing, wait until after the rich history has appeared in the official
repository.

Manual workflow
---------------

These instructions make the following assumptions:

1. You have the git-ubuntu **beta** snap installed.
2. A previous git-ubuntu commit is an ancestor of your current commit.
3. You have made a branch available at a public git URL on Launchpad that
   contains the rich history that will match your upload.
4. You are at the top level of your source tree that is ready to build.

Identify three items:

1. The publicly available URL on Launchpad to the git repository that contains
   your commit. This **must** begin with :code:`https://git.launchpad.net/`.
2. A ref under which your commit can be found. For example, if you have pushed
   to a branch called :code:`my-feature`, the ref would be
   :code:`refs/heads/my-feature`.
3. The full commit hash of your commit.

Call :code:`dpkg-buildpackage` as follows::

    dpkg-buildpackage -S \
      --changes-option=-DVcs-Git=<url> \
      --changes-option=-DVcs-Git-Ref=<ref> \
      --changes-option=-DVcs-Git-Commit=<commit>
      <your usual options>

Upload as usual::

    cd ..
    dput ubuntu <package>_<version>_source.changes

If you don't wish to retain your rich history branch in Launchpad, you must
wait for the importer to retrieve it before you delete it. To be sure of the
timing, wait until after the rich history has appeared in the official
repository.

Mangle workflow
---------------

These instructions make the following assumptions:

1. You have the git-ubuntu **beta** snap installed.
2. You have already built your source package and have the changes file ready
   to upload, except that it doesn't contain a rich history reference.
3. You have a local branch checked out, the checkout is identical to the source
   package you have prepared, and your working directory is clean.
4. A previous git-ubuntu commit is an ancestor of your current commit.
5. You are at the top level of your source tree that is already built.

Push your branch, adjust your changes file, re-sign it and upload as follows::

    dpkg-buildpackage $(git ubuntu prepare-upload mangle) ../<package>_<version>_source.changes
    cd ..
    debsign <package>_<version>_source.changes
    dput ubuntu <package>_<version>_source.changes

:code:`git ubuntu prepare-upload mangle` will push your branch to your
namespace on Launchpad with the same remote branch name, and then modify your
changes file to add the appropriate reference to the rich history, stripping
out the now-invalidated signature if it was present. See
:doc:`/reference/changes_file_headers`.

If you don't wish to retain your rich history branch in Launchpad, you must
wait for the importer to retrieve it before you delete it. To be sure of the
timing, wait until after the rich history has appeared in the official
repository.
