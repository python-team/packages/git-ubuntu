Configure keyring to use plaintext storage
------------------------------------------

See :doc:`/explanation/keyring` for details on how git-ubuntu uses keyring. If
you want to reconfigure keyring to use plaintext storage to avoid getting
keyring password prompts, create the file
``~/.local/share/python_keyring/keyringrc.cfg`` with the following contents::

    [backend]
    default-keyring=keyrings.alt.file.PlaintextKeyring

If you're not using the git-ubuntu snap then you need to have the
``python3-keyrings.alt`` package installed. Additionally, if you're not using
the snap and you are using Ubuntu 22.04 or later, then the file is located at
``~/.config/python_keyring`` instead, and the contents need to be something
like::

    [backend]
    default-keyring=keyrings.alt.file.PlaintextKeyring
    keyring-path=/home/ubuntu/.cache/keyring
