How-to guides
=============

.. toctree::

    upload
    handle-deletion
    check-update
    keyring
