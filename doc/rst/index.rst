git-ubuntu
==========

.. A single sentence that says what the product is, succinctly and memorably

git-ubuntu is tooling that provides unified git-based workflows for the
development of Ubuntu source packages.

.. A paragraph of one to three short sentences, that describe what the product
   does.

The git-ubuntu importer service maintains a view of the entire packaging
version history of Ubuntu source packages using git repositories with a common
branching and tagging scheme. The git-ubuntu CLI provides tooling and
automation that understands these repositories to make the development of
Ubuntu itself easier.

.. A third paragraph of similar length, this time explaining what need the
   product meets.

Since Ubuntu's packaging architecture pre-dates git, different packages have
evolved to use different mechanisms to achieve the same thing. Developers had
to learn them all in order to be effective when working across a wide range of
packages. git-ubuntu's unification of these mechanisms allows for simpler, more
general tooling, which results in faster onboarding of new developers and
easier "drive-by" contributions.

.. Finally, a paragraph that describes whom the product is useful for.

git-ubuntu is useful for those inspecting, working on, or looking to contribute
to Ubuntu source packages themselves.

----

In this documentation
---------------------

..  grid:: 1 1 2 2

   ..  grid-item::

   ..  grid-item:: :doc:`How-to guides <howto/index>`

      **Step-by-step guides** covering key operations and common tasks

.. grid:: 1 1 2 2
   :reverse:

   .. grid-item:: :doc:`Reference <reference/index>`

      **Technical information** - specifications, APIs, architecture

   .. grid-item:: :doc:`Explanation <explanation/index>`

      **Discussion and clarification** of key topics

----

Project and community
---------------------

git-ubuntu is a member of the Ubuntu family. It’s an open source project that
warmly welcomes community projects, contributions, suggestions, fixes and
constructive feedback.

* `Code of conduct <https://ubuntu.com/community/code-of-conduct>`_
* `Join our online chat <https://web.libera.chat/#ubuntu-devel>`_
* `View the source <https://git.launchpad.net/git-ubuntu>`_
* `Report bugs <https://bugs.launchpad.net/git-ubuntu/+filebug>`_

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    explanation/index
    howto/index
    reference/index
