SNAP_GIT_BASH_COMPLETION="/snap/git-ubuntu/current/usr/share/bash-completions/completions/git"
if [ -f $SNAP_GIT_BASH_COMPLETION ]; then
	. $SNAP_GIT_BASH_COMPLETION
fi

_git_ubuntu ()
{
	local subcommands="
		clone
		import
		merge
		queue
		remote
		submit
		tag
		"
	local all_opts="
		-h --help
		-l --pullfile
		-v --verbose
		-p --parentfile
		"
	local net_opts="
		--proto
		--retries
		--retry-backoffs
		"
	local subcommand="$(__git_find_on_cmdline "$subcommands")"
	if [ -z "$subcommand" ]; then
		__gitcomp "
			$subcommands
			$all_opts
			"
		return
	fi

	case "$subcommand,$cur" in
		clone,--*)
			__gitcomp "
				-l --lp-user
				$all_opts
				$net_opts
				"
			;;
		import,--*)
			__gitcomp "
				--active-series-only
                                --allow-applied-failures
				-d --directory
				--dl-cache
				-o --lp-owner
				-l --lp-user
				--no-clean
				--no-fetch
				--no-push
				--reimport
				--skip-applied
				--skip-orig
				$all_opts
				$net_opts
				"
			;;
		merge,--*)
			__gitcomp "
				start
				finish
				--bug
				-d --directory
				-f --force
				--release
				--tag-only
				$all_opts
				"
			;;
		queue,--*)
			__gitcomp "
				clean
				sync
				-d --directory
				--new
				--no-fetch
				--no-trim
				--orphan
				--parent
				--series
				--source
				--unapproved
				$all_opts
				$net_opts
				"
			;;
		remote,--*)
			__gitcomp "
				add
				-d --directory
				-l --lp-user
				--no-fetch
				--package
				-r --remote-name
				$all_opts
				$net_opts
				"
			;;
		tag,--*)
			__gitcomp "
				--bug
				-d --directory
				--split
				--format
				--logical
				--print-name-only
				--reconstruct
				--upload
				--force
				$all_opts
				"
			;;
	esac
}

if [ -a "`type -t __git_find_on_cmdline`" ]; then
	alias __git_find_on_cmdline=__git_find_subcommand
fi
