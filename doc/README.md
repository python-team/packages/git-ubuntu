## Running the git-ubuntu importer ##
This just covers how to run [`git ubuntu import`](https://code.launchpad.net/git-ubuntu).

## Getting via snap ##
The preferred installation method is to install via snap:

1. install the snap

        $ snap install --classic git-ubuntu

## [Alternate:]  Getting via git ##
Less well tested, but in theory this should work as well.

1. Get `git-ubuntu` from git

        $ git clone git://git.launchpad.net/git-ubuntu git-ubuntu
or

        $ git clone https://git.launchpad.net/git-ubuntu git-ubuntu
or

        $ git clone ssh://git.launchpad.net/git-ubuntu git-ubuntu
 

2. Put it in your PATH

        $ PATH="$PWD/git-ubuntu/bin:$PATH"

3. Get necessary dependencies

        $ sudo apt update -qy

        $ deps="dpkg-dev git-buildpackage python3-argcomplete \
          python3-lazr.restfulclient python3-debian python3-distro-info \
          python3-launchpadlib python3-pygit2 python3-ubuntutools \
          python3-cachetools python3-pkg-resources python3-pytest \
          python3-petname quilt"

        $ sudo apt install -qy ${deps}


## Running ##

 * For local usage

   `git ubuntu import` will push to launchpad git by default.  If you just want to get a git repo locally of a given package, then:

        $ mkdir ${HOME}/Imports
        $ PKG=uvtool
        $ git ubuntu import -v --no-push --directory=${HOME}/Imports/$PKG $PKG

 * As member of [git-ubuntu-import](https://launchpad.net/~git-ubuntu-import) for official publishing.

        $ PKG=uvtool
        $ LP_USER=smoser  # your launchpad user name if different from $USER
        $ git ubuntu import -v --directory=$PKG --lp-user=$LP_USER $PKG

## Shell completion (bash) ##

 * `git-ubuntu` will autocomplete by default if global argcomplete has
   been enabled
   (https://github.com/kislyuk/argcomplete#activating-global-completion)
   or specify

   eval "$(register-python-argcomplete git-ubuntu)"

   The snap version does this by default.

 * `git ubuntu` autocompletion is a little more challenging. To enable
   it, add the following to your .bashrc or similar:

   if [ -f /path/to/git-ubuntu/doc/gitubuntu-completion.sh ]; then
      . /path/to/git-ubuntu/doc/gitubuntu-completion.sh
   fi

   For the snap version, this would look like:

   if [ -f /snap/git-ubuntu/current/doc/gitubuntu-completion.sh ]; then
      . /snap/git-ubuntu/current/doc/gitubuntu-completion.sh
   fi

## View Output ##
If you did a local checkout with `--directory=./$PKG` then you'll have a git repository in `./$PKG/git`.

    $ cd $PKG
    $ git branch 
    ubuntu/saucy
    ubuntu/saucy-proposed
    ubuntu/trusty
    ubuntu/trusty-proposed
    ubuntu/utopic
    ubuntu/utopic-proposed
    ubuntu/vivid
    ubuntu/vivid-proposed
    ubuntu/wily
    ubuntu/xenial
    ubuntu/yakkety

If you did `--lp-owner=git-ubuntu-import`, then your repo should be
listed in web view at [https://code.launchpad.net/~git-ubuntu-import/+git].  And it should be able to be cloned with:

    $ git clone https://git.launchpad.net/~git-ubuntu-import/ubuntu/+source/$PKG
or

    $ git clone lp:~git-ubuntu-import/ubuntu/+source/$PKG
or

    $ git ubuntu clone $PKG


## Links ##
 * [GitWorkflow Wiki page](https://wiki.ubuntu.com/UbuntuDevelopment/Merging/GitWorkflow)
 * [Launchpad git for git-ubuntu](https://code.launchpad.net/git-ubuntu)
 * [Git view of git-ubuntu](https://git.launchpad.net/git-ubuntu)
